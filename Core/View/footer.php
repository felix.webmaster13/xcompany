<!-- Footer -->
<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <a href="http://fvsoftware.webcindario.com/" target="_blank">FVSoftware</a> 2020 | </span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->



<!------ Modal displayed if form it is submit---->
<div class="modal fade" id="MothlyPeriodValidationFailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-target="#MothlyPeriodValidationFailModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Validation Fail</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">No hay Períodos Disponibles.</div>
            <div class="modal-footer">
                <button id="btnYes" name="btnYes" class="btn btn-primary" type="button" data-dismiss="modal">OK</button>
                <!---<a class="btn btn-primary" href="../../index.php">Logout</a>-->
            </div>
        </div>
    </div>
</div>
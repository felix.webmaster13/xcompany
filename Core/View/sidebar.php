<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use app\Models\CompaniesModel;

use Libraries\Session;

$model = new CompaniesModel();

  $sesion = new Session();
  $user_id = $sesion -> get('user_id');

/*
 *
 * Filtros para los modulos maestros
 * 
 */


//print_r($datos);

?>

<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="cpanel.html">
        <div class="sidebar-brand-icon">
          <span class="rounded-circle"><img src="public/img/logooficialicon.png" height="50px" /></span>
        </div>
        <!--<div class="sidebar-brand-text mx-3"><span><img src="img/logooficial.jpg" height="20PX" /></span></div>-->
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php if(!empty($_SESSION['COMPANY_ID'])){ echo "?Task=CpanelController.index.".$_SESSION['COMPANY_ID']; } else {echo "#";} ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Configuration</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

             /*** 
             * 
             * Maestros 
             * 
             * 
             * ***/
            /*
             *
             * Filtros para los modulos configuraciones
             * 
             */

             $filters2 = new StdClass();
             $filters2 -> id_company = 1;
             $filters2 -> id_app_type = 1;
             $filters2 -> id_user = $user_id;

             $datos4 = $model -> getApplicationsByCompany($filters2);
          if( count($datos4)>0 ){

            for($i=0; $i<=count($datos4)-1;$i++){

              if( $datos4[$i]['name'] == "Applications"){
                echo '<h6 class="collapse-header">IT Configuration:</h6>';
              }

              if( $datos4[$i]['name'] == "Series NCF"){
                echo '<h6 class="collapse-header">Impuestos:</h6>';
              }
              echo '<a class="collapse-item" href="'.$datos4[$i]['url'].'">'.$datos4[$i]['name'].'</a>';
 
            }

          } else {
            echo '<a class="collapse-item" href="#"></a>';
          }
           

        ?>
         </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <!--<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Help</a>
            <a class="collapse-item" href="utilities-border.html">Contacts</a>
          </div>
        </div>
      </li>-->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <!--<div class="sidebar-heading">
        Addons
      </div>-->

      <!-- Nav Item - Pages Collapse Menu -->
      <!-- Nav Item - Start Transactions Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages2">
          <i class="fas fa-fw fa-calculator"></i>
          <span>Contabilidad</span>
        </a>
        <div id="collapsePages1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

        <?php

             /*** 
             * 
             * Transacciones 
             * 
             * 
             * ***/

            $filters5 = new StdClass();
            $filters5 -> id_company = 1;
            $filters5 -> id_app_type = 2;
            $filters5 -> id_user = $user_id;

            $datos5 = $model -> getApplicationsByCompany($filters5);

            if( count($datos5)>0 ){

              for($i=0; $i<=count($datos5)-1;$i++){

                echo '<a class="collapse-item" href="'.$datos5[$i]['url'].'">'.$datos5[$i]['name'].'</a>';

              }
            } else {
              echo '<a class="collapse-item" href="#"></a>';
            }
            
        ?>
          </div>
        </div>
      </li>  
      <!-- Nav Item - End Transactions Menu -->
      <!-- Nav Item - Start Transactions Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages2">
          <i class="fas fa-fw fa-hand-holding-usd"></i>
          <span>Cuentas por Cobrar</span>
        </a>
        <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

             /*** 
             * 
             * Transacciones 
             * 
             * 
             * ***/

            $filters = new StdClass();
            $filters -> id_company = 1;
            $filters -> id_app_type = 3;
            $filters -> id_user = $user_id;
            
            $datos = $model -> getApplicationsByCompany($filters);


            if( count($datos)>0 ){

              for($i=0; $i<=count($datos)-1;$i++){

                echo '<a class="collapse-item" href="'.$datos[$i]['url'].'">'.$datos[$i]['name'].'</a>';

              }
            } else {
              echo '<a class="collapse-item" href="#"></a>';
            }

        ?>
          </div>
        </div>
      </li>  
      <!-- Nav Item - End Transactions Menu -->
      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages3" aria-expanded="true" aria-controls="collapsePages2">
          <i class="fas fa-fw fa-money-bill"></i>
          <span>Cuentas por Pagar</span>
        </a>
        <div id="collapsePages3" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

             /*** 
             * 
             * Maestros 
             * 
             * 
             * ***/
            /*
             *
             * Filtros para los modulos maestros
             * 
             */

             $filters2 = new StdClass();
             $filters2 -> id_company = 1;
             $filters2 -> id_app_type = 4;
             $filters2 -> id_user = $user_id;

             $datos2 = $model -> getApplicationsByCompany($filters2);
            
            if( count($datos2)>0 ){
              for($i=0; $i<=count($datos2)-1;$i++){

                echo '<a class="collapse-item" href="'.$datos2[$i]['url'].'">'.$datos2[$i]['name'].'</a>';

              }
            } else {
              echo '<a class="collapse-item" href="#"></a>';
            }

        ?>
          </div>
        </div>
      </li>  
      <!-- Nav Item - End Maestros Menu -->
      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages4" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-landmark"></i>
          <span>Bancos</span>
        </a>
        <div id="collapsePages4" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
               
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters3 = new StdClass();
                $filters3 -> id_company = 1;
                $filters3 -> id_app_type = 5;
                $filters3 -> id_user = $user_id;

                $datos3 = $model -> getApplicationsByCompany($filters3);

                if( count($datos2)>0 ){
                  for($i=0; $i<=count($datos3)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos3[$i]['url'].'">'.$datos3[$i]['name'].'</a>';
    
                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

        ?>
          </div>
        </div>
      </li>  

      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages5" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-box"></i>
          <span>Inventario</span>
        </a>
        <div id="collapsePages5" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
               
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters3 = new StdClass();
                $filters3 -> id_company = 1;
                $filters3 -> id_app_type = 6;
                $filters3 -> id_user = $user_id;

                $datos3 = $model -> getApplicationsByCompany($filters3);

                if( count($datos2)>0 ){
                  for($i=0; $i<=count($datos3)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos3[$i]['url'].'">'.$datos3[$i]['name'].'</a>';
    
                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

        ?>
          </div>
        </div>
      </li>  

      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages6" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-project-diagram"></i>
          <span>Proyectos</span>
        </a>
        <div id="collapsePages6" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
               
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters3 = new StdClass();
                $filters3 -> id_company = 1;
                $filters3 -> id_app_type = 7;
                $filters3 -> id_user = $user_id;

                $datos3 = $model -> getApplicationsByCompany($filters3);

                if( count($datos2)>0 ){
                  for($i=0; $i<=count($datos3)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos3[$i]['url'].'">'.$datos3[$i]['name'].'</a>';
    
                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

        ?>
          </div>
        </div>
      </li>  

      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages7" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fas fa-coins"></i>
          <span>Activos Fijos</span>
        </a>
        <div id="collapsePages7" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
               
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters3 = new StdClass();
                $filters3 -> id_company = 1;
                $filters3 -> id_app_type = 8;
                $filters3 -> id_user = $user_id;

                $datos3 = $model -> getApplicationsByCompany($filters3);

                if( count($datos2)>0 ){
                  for($i=0; $i<=count($datos3)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos3[$i]['url'].'">'.$datos3[$i]['name'].'</a>';
    
                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

        ?>
          </div>
        </div>
      </li>

      <!-- Nav Item - Start Maestros Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages8" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-dollar-sign"></i>
          <span>Nomina</span>
        </a>
        <div id="collapsePages8" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
               
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters3 = new StdClass();
                $filters3 -> id_company = 1;
                $filters3 -> id_app_type = 9;
                $filters3 -> id_user = $user_id;

                $datos3 = $model -> getApplicationsByCompany($filters3);

                if( count($datos2)>0 ){
                  for($i=0; $i<=count($datos3)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos3[$i]['url'].'">'.$datos3[$i]['name'].'</a>';
    
                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

        ?>
          </div>
        </div>
      </li>    


      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages9" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-chart-pie"></i>
          <span>Reportes</span>
        </a>
        <div id="collapsePages9" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

          <?php

            /*** 
                 * 
                 * Reportes 
                 * 
                 * 
                 * ***/
              
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters4 = new StdClass();
                $filters4 -> id_company = 1;
                $filters4 -> id_app_type = 11;
                $filters4 -> id_user = $user_id;

                $datos4 = $model -> getApplicationsByCompany($filters4);

                if( count($datos4)>0 ){
                  for($i=0; $i<=count($datos4)-1;$i++){

                    if( $datos4[$i]['name'] == "606"){
                      echo '<h6 class="collapse-header">Impuestos:</h6>';
                    }

                    echo '<a class="collapse-item" href="'.$datos4[$i]['url'].'">'.$datos4[$i]['name'].'</a>';

                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

            ?>

          </div>
        </div>
      </li>  
      

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages10" aria-expanded="true" aria-controls="collapsePages3">
          <i class="fas fa-fw fa-upload"></i>
          <span>Importar</span>
        </a>
        <div id="collapsePages10" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

          <?php

            /*** 
                 * 
                 * Imports 
                 * 
                 * 
                 * ***/
              
              /*
                *
                * Filtros para los modulos de reportes
                * 
                */

                $filters4 = new StdClass();
                $filters4 -> id_company = 1;
                $filters4 -> id_app_type = 10;
                $filters4 -> id_user = $user_id;

                $datos4 = $model -> getApplicationsByCompany($filters4);

                

                if( count($datos4)>0 ){
                  for($i=0; $i<=count($datos4)-1;$i++){

                    echo '<a class="collapse-item" href="'.$datos4[$i]['url'].'">'.$datos4[$i]['name'].'</a>';

                  }
                } else {
                  echo '<a class="collapse-item" href="#"></a>';
                }

            ?>

          </div>
        </div>
      </li>  
      
      <!-- Nav Item - Charts -->
      <!--<li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>-->

      <!-- Nav Item - Tables -->
      <!--<li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
<!-- Bootstrap core JavaScript-->
  <script src="public/vendor/jquery/jquery.min.js"></script>

  <!-- JQuery Mask-->
  <script src="public/vendor/jquerymask/dist/jquery.mask.min.js"></script>
  
  <script src="public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  

  <!-- Core plugin JavaScript-->
  <script src="public/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="public/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="public/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="public/js/demo/chart-area-demo.js"></script>
  <script src="public/js/demo/chart-pie-demo.js"></script>

  <!-- Custom scripts for kendo-->
  <script src="public/vendor/kendo/js/jszip.min.js"></script>
  <script src="public/vendor/kendo/js/kendo.all.min.js"></script>

  <!-- FlexDataList JQuery-->
  <script src="public/vendor/flexDataList/jquery.flexdatalist.min.js"></script>

  <!-- JQuery Validator-->
  <script src="public/vendor/jquery-validator/jquery.validate.min.js"></script>


  
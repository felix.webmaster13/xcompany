<?php
namespace Libraries;

use fpdf\fpdf;
use \DateTime;

use app\Models\PrintSalidaModel;
use app\Models\PrintQuoteModel;
use app\Models\CreateCompaniesModel;
//use \StdClass;


/*
 * Class to display the main HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 22 de Abril del 2020 
 * 
 */

Class PDFCommonView {

    public $template;
    public $subtotal=0;
    public $grandsubtotal=0;

    public function body($data){

        //print_r($data);

       //echo $id_invoice;

        //$filters = new StdClass();
        //Get Company Information
        $company = new CreateCompaniesModel();
        $datacompany = $company -> getById(1);

        if( $data -> module == "FACTURA"){
            //Get Invoice data
            $instdatos = new PrintSalidaModel();
            $datos = $instdatos -> getById($data -> invoice_id);

            //Variables
            $title = "FACTURA";
            $NCF = $datos[0]['ncf'];
            $NCF_valido_hasta = $datos[0]['ncf_valido_hasta'];

        } else if( $data -> module == "COTIZACION"){
            //Get Quote data
            $instdatos = new PrintQuoteModel();
            $datos = $instdatos -> getById($data -> invoice_id);

            //Variables
            $title = "COTIZACION";
            $NCF = 00000000000000000;
            $NCF_valido_hasta = date("Y-m-d");
            //echo $NCF_valido_hasta;
        }
        

        //print_r($datos);
        
        require('../public/vendor/fpdf/fpdf.php');

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',36);
        $pdf->SetFillColor(200, 200, 200);
        
        
        //Linea Divisora
        $pdf->Cell(190,15,'', 'T', 1, 1, 0);
        
        // Title
        $pdf->Cell(190,15,$title, 0, 1, 'C', 0);

        //Salto de Linea
        $pdf->Ln();

        // Company Information
        $pdf->SetFont('Arial','',16);
        $pdf->SetXY(10,55);
        $pdf->Cell(90,8,utf8_decode($datacompany[0]['name']), 0, 1, 'L', 0);
        $pdf->SetXY(10,65);
        $pdf->Cell(90,8,"RNC: ".$datacompany[0]['rnc'], 0, 1, 'L', 0);
        $pdf->SetXY(10,75);
        $created_at = new DateTime($datos[0]['fecha']);
        $pdf->Cell(90,8,"Fecha: ".date_format($created_at,"d/m/Y"), 0, 1, 'L', 0);

        if( $data -> module == "FACTURA"){
            
            // DGII Information
            $pdf->SetFont('Arial','B',20);
            $pdf->SetXY(110,55);
            $pdf->Cell(90,8,utf8_decode('Factura de Crédito Fiscal'), 0, 1, 'R', 0);
            $pdf->SetFont('Arial','',16);
            $pdf->SetXY(110,65);
            $pdf->Cell(90,8,"NCF: ".$NCF, 0, 1, 'R', 0);
            $pdf->SetXY(110,75);
            $created_at2 = new DateTime($NCF_valido_hasta);
            $pdf->Cell(90,8,utf8_decode("Válida Hasta: ".date_format($created_at2,"d/m/Y") ), 0, 1, 'R', 0);

        }

        //Salto de Linea
        $pdf->Ln();

        //Linea Divisora
        $pdf->Cell(190,15,'', 'T', 1, 1, 0);

        // Client Information
        $pdf->SetFont('Arial','',16);
        $pdf->SetXY(10,95);
        $pdf->Cell(90,8,"RNC Cliente: ".$datos[0]['rnc_cliente'], 0, 1, 'L', 0);
        $pdf->SetXY(10,105);
        $pdf->Cell(90,8,utf8_decode($datos[0]['name_cliente']), 0, 1, 'L', 0);

        //Salto de Linea
        $pdf->Ln();

        // Details Header
        $pdf->SetFont('Arial','B',12);
        $pdf->SetXY(10,125);
        $pdf->Cell(20,8,utf8_decode("Código"), 1, 1, 'C', 1);
        $pdf->SetXY(30,125);
        $pdf->Cell(65,8,utf8_decode("Descripción"), 1, 1, 'C', 1);
        $pdf->SetXY(95,125);
        $pdf->Cell(20,8,utf8_decode("Cantidad"), 1, 1, 'C', 1);
        $pdf->SetXY(115,125);
        $pdf->Cell(25,8,utf8_decode("Precio"), 1, 1, 'C', 1);
        $pdf->SetXY(140,125);
        $pdf->Cell(30,8,utf8_decode("Itbis"), 1, 1, 'C', 1);
        $pdf->SetXY(170,125);
        $pdf->Cell(30,8,utf8_decode("Valor"), 1, 1, 'C', 1);

    $Y1 = 133;  
    $subtotal = 0;
    $total_itbis = 0;
    $itbis = 0;  
    $valor = 0;  
        for($i=0;$i<=count($datos)-1;$i++){
            //print_r($datos[$i]);
            //foreach($datos[$i] as &$valor){
                //echo $valor."<br />";

                //Iteration Details
                $pdf->SetFont('Arial','',10);
                $pdf->SetXY(10,$Y1);
                $pdf->Cell(20,8,utf8_decode($datos[$i]['code']), 1, 1, 'C', 0);
                $pdf->SetXY(30,$Y1);
                $pdf->Cell(65,8,utf8_decode($datos[$i]['name']), 1, 1, 'C', 0);
                $pdf->SetXY(95,$Y1);
                $pdf->Cell(20,8,number_format($datos[$i]['cantidad'],2), 1, 1, 'R', 0);
                $pdf->SetXY(115,$Y1);
                $pdf->Cell(25,8,number_format($datos[$i]['precio'],2), 1, 1, 'R', 0);
                $pdf->SetXY(140,$Y1);
                $pdf->Cell(30,8,number_format( ( ($datos[$i]['cantidad']*$datos[$i]['precio']) * $datos[$i]['itbis']),2), 1, 1, 'R', 0);
                $pdf->SetXY(170,$Y1);
                $pdf->Cell(30,8,number_format( ($datos[$i]['cantidad']*$datos[$i]['precio']),2), 1, 1, 'R', 0);

                $Y1 = $Y1 + 8;
                $itbis =  ( ($datos[$i]['cantidad']*$datos[$i]['precio']) * $datos[$i]['itbis']);
                $total_itbis = $total_itbis + $itbis;
                $valor = ($datos[$i]['cantidad']*$datos[$i]['precio']);
                $subtotal = $subtotal + $valor;
                

            //}
        }
        
        $Y2 = $Y1 + 10;
        
        //Salto de Linea
        $pdf->Ln();

        //Linea Divisora
        $pdf->SetFont('Arial','',12);
        $pdf->SetXY(10,$Y2);
        $pdf->Cell(190,15,'', 'T', 1, 1, 0);


        // Resume Detail Calculations
        $pdf->SetFont('Arial','',16);
        $pdf->SetXY(110,170);
        $pdf->Cell(60,8,"SUBTOTAL:", 0, 1, 'R', 0);
        $pdf->SetXY(110,180);
        $pdf->Cell(60,8,"DESC.:", 0, 1, 'R', 0);
        $pdf->SetXY(110,190);
        $pdf->Cell(60,8,"ITBIS:", 0, 1, 'R', 0);
        $pdf->SetFont('Arial','B',16);
        $pdf->SetXY(110,200);
        $pdf->Cell(60,8,"TOTAL:", 0, 1, 'R', 0);


        $pdf->SetFont('Arial','',14);
        $pdf->SetXY(170,170);
        $pdf->Cell(30,8,number_format($subtotal,2), 0, 1, 'R', 0);
        $pdf->SetXY(170,180);
        $pdf->Cell(30,8,"0.00", 0, 1, 'R', 0);
        $pdf->SetXY(170,190);
        $pdf->Cell(30,8,number_format($total_itbis,2), 0, 1, 'R', 0);
        $pdf->SetFont('Arial','B',16);
        $pdf->SetXY(170,200);
        $pdf->Cell(30,8,number_format( ($subtotal + $total_itbis) ,2), 0, 1, 'R', 0);
        
        $pdf->Output();

        
    }
  
}


?>
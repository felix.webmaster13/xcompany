<?php
namespace Libraries;

use app\Models\SalidaTIcketModel;
use \StdClass;


/*
 * Class to display the main HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 22 de Abril del 2020 
 * 
 */

Class TicketCommonView {

    public $template;
    public $subtotal=0;
    public $grandsubtotal=0;

    public function body($id_invoice){

        $filters = new StdClass();
        $getSalida = new SalidaTIcketModel();


        $filters -> id_salida = $id_invoice;

        $datos = $getSalida -> getReceiptByFilters( $filters );

        //print_r($datos)."<br />";
        //echo count($datos)."<==========Total registros";

        $this -> StartTableDefinition();

        //Ticket Top
            $this -> Logo();
            $this -> CompanyName();
            $this -> CompanyAddress();
            $this -> CompanyPhone();

            $this -> BlankLine();

        //Tikect Header
            $this -> DateandTIme( $datos[0]['fecha'] );
            $this -> InvoiceNumber($datos[0]['id_salida']);
            $this -> Client($datos[0]['cliente']);
            $this -> SalesBy($datos[0]['user_id']);

        //Tickect Details    
            $this -> DetaiilsSeparator();

            for($i=0;$i<=count($datos)-1;$i++){

                
                //Display many times depends in the number of items
                    $this -> ItemsDetails($datos[$i]['code'], $datos[$i]['name'] , $datos[$i]['imei'] , $datos[$i]['precio'] , $datos[$i]['itbis'] );


            }
            
                $this -> ItemsSeparator();

                $this -> GrandTotal();

            $this -> BlankLine();

            $this -> BottomMessage();

        $this -> EndTableDefinition();



    }

    public function StartTableDefinition($title = '', $html = ''){

       echo '<div class="container text-center" width="100%">

                <table style="text-align: center; border: 10px solid;" width="20%" border="0">';
    }

    public function Logo(){

        echo '  <tr>
                    <td colspan="4"><img src="public/img/reyessmartphones.jpeg" width="50%" height="10%" /></td>
                </tr>';

    }


    public function CompanyName(){

        echo '  <tr>
                    <td colspan="4"> <h2>Reyes SmartPhones</h2> </td>
                </tr>';

    }

    public function CompanyAddress(){

        echo '   <tr>
                    <td colspan="4"> <p>Av. Yapour Dumit Centro comercial D\' Leon</p> </td>
                </tr>';

    }

    public function CompanyPhone(){

        echo '   <tr> 
                    <td align="left" colspan="2"> Teléfono: </td>
                    <td align="left" colspan="2"> 829-549-0599 </td>
                </tr>';

    }

    public function BlankLine(){

        echo '   <tr> 
                    <td colspan="4"> &nbsp; </td>
                </tr>';

    }

    public function DateandTIme( $fecha ){

        echo '   <tr> 
                    <td align="left"> Fecha: </td>
                    <td align="left"> '.$fecha.' </td>
                    <td align="left"> Hora: </td>
                    <td align="left"> '.date("H:i:s").' </td>
                </tr>';

    }

    public function InvoiceNumber( $id_invoice ){

        echo '   <tr> 
                    <td align="left"> Factura #: </td>
                    <td align="left" colspan="3"> '.$id_invoice.' </td>
                </tr>';

    }

    public function Client($cliente){

        echo '   <tr> 
                    <td align="left"> Cliente: </td>
                    <td align="left" colspan="3"> '.$cliente.'  </td>
                </tr>';

    }

    public function SalesBy($user){

        echo '   <tr> 
                    <td align="left"> Vendedor: </td>
                    <td align="left" colspan="3"> '.$user.'  </td>
                </tr>';

    }

    public function DetaiilsSeparator(){

        echo '  <tr> 
                    <td colspan="4">==================================</td>
                </tr>
                <tr> 
                    <td> <b>Código:</b> </td>
                    <td colspan="3"><b>Descripción</b>  </td>
                </tr>
                <tr> 
                    <td colspan="4">==================================</td>
                </tr>';

    }

    
    public function ItemsDetails( $code, $name, $imei, $precio, $itbis){

        

        echo '  <tr> 
                    <td> '.$code.' </td>
                    <td colspan="3">'.$name.'</td>
                </tr>
                <tr> 
                    <td> IMEI: </td>
                    <td colspan="3">'.$imei.'</td>
                </tr>
                <tr> 
                    <td> Precio: </td>
                    <td colspan="3" align="right">'.number_format($precio,2).'</td>
                </tr>
                <tr> 
                    <td> ITBIS: </td>
                    <td colspan="3" align="right">'.number_format($itbis,2).'</td>
                </tr>';
                
                $this -> subtotal = ($precio+$itbis);
                $this -> grandsubtotal += ($precio+$itbis);

        echo '  <tr> 
                    <td> <b>SubTotal</b> </td>
                    <td colspan="3" align="right"> <b>'.number_format($this -> subtotal,2).'</b></td>
                </tr>';

    }

    public function ItemsSeparator(){

        echo '  <tr> 
                    <td colspan="4">---------------------------------------------------------</td>
                </tr>';

    }

    public function GrandTotal(){
        echo '  <tr> 
                    <td> <b>Grand Total</b> </td>
                    <td colspan="3" align="right"> <b>RD$ '.number_format($this -> grandsubtotal,2).'</b></td>
                </tr>';
    }

    public function BottomMessage(){

        echo '  <tr> 
                    <td colspan="4"> <b>30 días de garantia en los equipos, sin caida, sin mojdas. El mismo pierde la garantia en su totalidad.</b> </td>
                </tr>';

    }
    

    public function EndTableDefinition($title = '', $html = ''){

        echo '</table>';
     }

      

}


?>
<?php
namespace Libraries;

/*
 *
 * Class to control the user access in the aplicación
 * Author: FVSoftware - Felix Valerio
 * Date: 22 de Abril de 2020
 * 
 */
Class Session{

    //Start PHP Session
    public function initialize(){
        session_start();
    }

    /*
     *
     * Add elemet into a PHP Session 
     * @param string $key of the array session
     * @param string $value to be asigned to the key 
     * 
     */
    public function addToSession($key, $value){
        $_SESSION[$key] = $value;
    }


    /*
     *
     * Return an element to a PHP Session 
     * @param string $key of the array session
     * @return string value of the array session if have value 
     * 
     */
    public function get($key){
        return !empty($_SESSION[$key]) ? $_SESSION[$key] : null ;
    }

    /*
     *
     * Return all values of the PHP Session 
     * @return completed array
     * 
     */
    public function getAll(){
        return $_SESSION;
    }

    /*
     *
     * Remove a specific element of the session
     * @param string $key of the array session
     * 
     */
    public function remove($key){
        if( !empty($_SESSION[$key]) ){
            unset($_SESSION[$key]);
        }
    }

    /*
     *
     * Close session and remove all values
     * 
     */
    public function close(){
        session_unset();
        session_destroy();
    }

    /*
     *
     * Return session status
     * @return string status of the session
     * 
     */
    public function getStatus(){
        return session_status();
    }


}

?>


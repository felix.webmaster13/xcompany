<?php
namespace Libraries;

use \DateTime;

/*
 *
 * Class to secure get values for the form
 * Autor: FVSoftware - Felix Valerio
 * Date: 16 de Abril 2020
 * 
 */
Class Input{

    protected $reg_exp = '';

    public function __contruct(){

    }

    //to capture integer values
    public function getInteger($value){

        $value = $_POST[$value];

        $this -> reg_exp = '/^[0-9]*$/';

        if (!preg_match($this -> reg_exp, $value)) {
            // Error
            //echo "Esta dato no es integer";
            return 0;
        } else {
            // Continue
            //echo "Esta dato si es integer";
            return $value;
        }
        
    }

    //to capture Float values
    public function getFloat($value){

        $value = $_POST[$value];

        //$this -> reg_exp = '/^[0-9]+(\.[0-9]+)?$/';
        $this -> reg_exp = '/^-?[0-9]\d*(\.\d+)?$/';

        if (!preg_match($this -> reg_exp, $value)) {
            // Error
            //echo "Esta dato no es flotante";
            return 0;
        } else {
            // Continue
            //echo "Esta dato si es flotante";
            return $value;
        }
        
    }

    //to capture string values
    public function getString($value){

        //print_r($_POST);
        //echo $_POST[$value];
        $value = $_POST[$value];
        
        $findme   = ' ';
        $pos = strpos($value, $findme);

        //echo $value;
        //echo gettype($value);
        /*if ($pos === false) {
            $this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        } else {
            $this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*\s+([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        }*/
        //$this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*\s+([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        
        $this -> reg_exp = '/([a-zA-Z0-9%\.\._-])$/';

        if (!preg_match($this -> reg_exp, $value)) {
            // Error
            // "Esta dato no es character";
            return '';
        } else {
            // Continue
            //echo "Esta dato si es character";
            $value = strtoupper($value);
            return $value;
        }
        
    }


    //to capture string values
    public function getDates($value){

        //print_r($_POST);
        //echo $_POST[$value];
        
        $created_at = new DateTime($_POST[$value]);

        //echo date_format($created_at,'F d,Y');
        //echo "here";
        $value = date_format($created_at,'Y-m-d');
        
        $findme   = ' ';
        $pos = strpos($value, $findme);

        

        //echo $value;
        //echo gettype($value);
        /*if ($pos === false) {
            $this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        } else {
            $this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*\s+([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        }*/
        //$this -> reg_exp = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*\s+([a-zA-Z0-9])+([a-zA-Z0-9\._-])$/';
        
        $this -> reg_exp = '/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';

        if (!preg_match($this -> reg_exp, $value)) {
            // Error
            //echo "Esta dato no es fecha";
            return '';
        } else {
            // Continue
            //echo "Esta dato si es fecha";
            $value = strtoupper($value);
            return $value;
        }
        
    }

    //to capture Arrays types integer values
    public function getArray($value){

        $value = $_POST[$value];

        $this -> reg_exp = '/\[[0-9,]+[0-9]*\]/';

        if (!preg_match_all($this -> reg_exp, $value )) {
            // Error
            //echo "Esta dato no es integer";
            return 0;
        } else {
            // Continue
            //echo "Esta dato si es integer";
            return $value;
        }
        
    }

    

}

?>
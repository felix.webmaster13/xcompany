<?php
namespace Libraries;

use Libraries\CommonView;
use Libraries\DBDriver;

/*
 *
 * Main Controller for the application
 * Autor: FVSoftware - Felix Valerio
 * Functionality: Responsable for load all Models and Views
 * Date: 10 de Abril 2020
 * 
 */

 
Class Controller extends Input{
    
    protected $currentView = "";

    //Load Models
    public function Model($modelName){
        //echo $modelName;
        //Loading
        require_once '../app/Models/'. $modelName . '.php';
        //create a new Object of the modelName
        $db = new $modelName();
        return $db;
    }

    //Load Views
    public function View($viewName, $data = []){
       // echo "here2";
        //Review if file exist
        //echo '../app/Views/'. $viewName . '.php';
        
        if( file_exists('../app/Views/'. $viewName . '.php') ){
           //echo "Yes"; 
            $this -> currentView = ucwords($viewName);

            
            if($viewName <> "loginView"){

                $common = new CommonView();

           }
            
            
            //Loading
            require_once '../app/Views/'. ucwords($viewName) . '.php';

            $this -> currentView = new $this -> currentView;
           //echo $viewName;
            return $this -> currentView -> index($viewName, $data);

        } else {
            die("The View file do not exist");
        }

    }

    //Delete temporal tables
    public function ClearTMP($user_id, $company_id, $table){

        //echo "Este es el user id==================>".$user_id;
        $db = new DBDriver();

        $sql="DELETE FROM ".$table." WHERE user_id=".$user_id;
                    //echo $sql;
                    //exit;
        $db -> setQuery($sql);
        $db -> execute();

    }

    //Delete temporal tables
    public function TruncateTMP($table){

        //echo "Este es el user id==================>".$user_id;
        $db = new DBDriver();

        $sql="TRUNCATE TABLE ".$table." ";
                    //echo $sql;
                    //exit;
        $db -> setQuery($sql);
        $db -> execute();

    }


    //Delete temporal tables
    public function InsertDefaultRowsTMP($user_id, $company_id, $table){

        //echo "Este es el user id==================>".$user_id;
        $db2 = new DBDriver();
        $i=0;
        for($i=1;$i<=6;$i++){

            $sql2="INSERT INTO ".$table." (user_id, company_id) VALUES (".$user_id.", ".$company_id.") ";
                        //echo $sql2;
                        //exit;
            $db2 -> setQuery($sql2);
            $db2 -> execute();

        }
        

    }


    public function RedirectSessionDeny(){

        \header("Location: ".PATH_URL);

    }

}


?>
<?php
namespace Libraries;

use PDO;


/*
 *
 * Main Class to connect into a Database
 * Author: FVSoftware - Felix Valerio
 * Date: 13 de Abril 2020
 *  
 */
class DBDriver {
    
    private $host   = DB_HOST;
    private $user   = DB_USER;
    private $pass   = DB_PASS;
    private $dbName = DB_NAME;

    //Database Handled
        private $dbh;
    //Execute Query
        private $stmt;
    //Error
        private $error;

        private $question;
        private $fields;

    public function __construct(){

        //Configuration for Conexion
        //$dsn = 'mysql:host='. $this->host . ';dbname=' . $this->dbName; 
        $dsn = 'mysql:host='. $this->host ; 

        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        //Creating a new Object of PDO
        try{

            $this -> dbh = new PDO($dsn, $this -> user, $this -> pass, $options);
            //Working with the special characters as (ó á í ú é ñ) 
            $this -> dbh -> exec('set names utf8');

        } catch (PDOException $e) {
            $this -> error = $e -> getMessage();
            echo $this -> error;
        }

    }

    //Method to prepare a qUery into a Database
    public function setQuery($sql){

        $this -> stmt = $this -> dbh -> prepare($sql);

    }

    //Method to prepare an Insert into a Database
    public function insertar($db, $fields = []){

        //print_r($fields);

        foreach($fields as &$valor){
            $this -> question.= "'".$valor."',";
            //echo $valor.",";
        }
        //echo $this -> question;
        $this -> question = substr($this -> question,0,-1);
        //exit;
        foreach($fields as $key => $valor){
            $this -> fields.= $key.",";
            //echo $valor.",";
        }
        

        $this -> fields = substr($this -> fields,0,-1);
        //echo "INSERT INTO ".$db." (".$this -> fields.") VALUES (".$this -> question.") ";
        //exit;
        $this -> stmt = $this -> dbh -> prepare("INSERT INTO ".$db." (".$this -> fields.") VALUES (".$this -> question.") ");

        

    }

    //Method to prepare a Update for a table in a Database
    public function actualizar($db, $fields = [], $wheredata){

        //print_r($fields);
        //exit;
        foreach($fields as $key => $valor){
            $this -> fields.= $key."='".$valor."',";
            //echo $key."=".$valor.",";
        }

        $this -> fields = substr($this -> fields,0,-1);
        //echo "UPDATE ".$db." SET ".$this -> fields." WHERE ".$wheredata;
        //exit;
        $this -> stmt = $this -> dbh -> prepare("UPDATE ".$db." SET ".$this -> fields." WHERE ".$wheredata);

    }


    //Method to prepare a Delete for a table in a Database
    public function Borrar($db, $wheredata){

        //echo "DELETE FROM ".$db." WHERE ".$wheredata;
        $this -> stmt = $this -> dbh -> prepare("DELETE FROM ".$db." WHERE ".$wheredata);

    }

   
    //Method to start a transaction in the database table
    public function beginTransaction(){
        $this -> dbh -> beginTransaction();
    }

     //Method to RollBack a transaction in the database table
     public function rollbackTransaction(){
        $this -> dbh -> rollBack();
    }

    //Method to Commit a transaction in the database table
    public function commitTransaction(){
        $this -> dbh -> commit();
    }

    //Method to Bind a qUery into a Database
    public function bind($field, $value, $dataType = null ){

        //Start Reviewing if $dataType Params it is null
        if(is_null($dataType)){

            switch (true){
                case is_int($value):
                    $tipo = PDO::PARAM_INT;
                break;
                case is_bool($value):
                    $tipo = PDO::PARAM_BOOL;
                break;
                case is_null($value):
                    $tipo = PDO::PARAM_NULL;
                break;
                
                default:
                    $tipo = PDO::PARAM_STR;
                break;

            }

        }
        //End Reviewing if $dataType Params it is null

        $this -> stmt -> bindValue($field, $value, $dataType);

    }


    //Method to execute the query
    public function execute(){
        
        return $this -> stmt -> execute();
    }

    //Getting all records from the query
    public function getObjectList(){
        $this -> execute();
        return $this -> stmt -> fetchAll(PDO::FETCH_ASSOC);
    }

    //Getting an specific record from the query
    public function getObject(){
        $this -> execute();
        return $this -> stmt -> fetch(PDO::FETCH_ASSOC);
    }

     //Getting te total of records from the query
     public function getCountOfRecords(){
        return $this -> stmt -> rowCount();
    }

}


?>
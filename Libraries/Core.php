<?php
namespace Libraries;

use app\Controllers\IndexController;

/*
 *
 * Get URL typed on the browser
 * Components:
 * 1- Controller
 * 2- Method
 * 3- Param
 * 4- Example: /items/update/4
 * 
 */

 Class Core{

    Protected $actualController = "indexController";
    Protected $actualMethod = "index";
    Protected $params = [];

   //Contructor
   public function __construct(){
        $url = $this -> getURL();
       
      /*** Extracting the controller from the URL ***/

         //Reviewing if file exist in Controllers Folders
         //echo $url."here";
         //echo '../app/Controllers/'.ucwords($url[0]).'.php'."<br />";
         //exit;
         if( file_exists('../app/Controllers/'.ucwords($url[0]).'.php') ){
            
               //if Exists will be the defect Controller
                  $this -> actualController = ucwords($url[0]);

               //unset Controller
               unset($url[0]);
         } else {
            //echo "no existe";
         }

        // echo '../app/Controllers/'.ucwords($this -> actualController).'.php'."</br>";
         require_once '../app/Controllers/'.ucwords($this -> actualController).'.php';
         //echo $this -> actualController;
         $this -> actualController = new $this -> actualController;
          
      
      /*** End Extracting the controller from the URL ***/


      /*** Extracting the Method from the URL ***/

         //Reviewing in the request exist the method
         if( isset($url[1]) ){
           
            //Reviewing if method exist inside the Class
            if( method_exists($this->actualController, $url[1]) ){
            
               //if Exists will be the defect Controller
                  $this -> actualMethod = $url[1];
                  //unset Method
                  unset($url[1]);
   
            }

            //return the ActualMethod
            //echo $this->actualMethod;

         }  
          
      /*** End Extracting the Method from the URL ***/


      /*** Extracting the Params from the URL ***/

         //Getting params from the URL
         $this->params = $url ? array_values($url) : [];

         //Callback with array Params
         call_user_func_array([$this->actualController, $this->actualMethod],$this->params);
          
      /*** End Extracting the Params from the URL ***/
      
      //return the ActualController 
     
      return $this -> actualController;

   }

    public function getURL(){
       // if URL Exist
       if( isset($_GET["Task"]) ){
            $url = rtrim($_GET["Task"],".");
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('.', $url);
            return $url;
       }
    }

 }



?>
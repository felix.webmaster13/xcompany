<?php
namespace Libraries;

use Libraries\phpQuery;


/*
 * Class to display the main HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 22 de Abril del 2020 
 * 
 */

Class CommonView{

    public $template; 

    public function template($title = '', $html = ''){

        $contentfile = file_get_contents('../public/app/'. $html . '.html');
        $this -> template = phpQuery::newDocument($contentfile, 'text/html');
        //echo $template;

        $this -> header();

        $this -> boddy($title, $this -> template);
    }

    public function header(){
        
        
        echo '<!DOCTYPE html>
        <html lang="en">
        
        <head>
        
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="Business Softwares">
          <meta name="author" content="FVSoftware">
        
          <title>FVSoftware - System</title>
          <link href="public/img/logooficialicon.png" rel="icon">
        
          <!-- Custom fonts for this template-->
          <!-- Custom styles for this template-->';
          
          include "../Core/View/cssfiles.php";
        
        
    }

    public function boddy( $title = '', $html = '' ){
        echo '<body id="page-top">';

        $this -> wrapper($title, $html);

        $this -> logoutModal();

        $this -> includeScripts( $title );

        echo '</body>';
    }

    public function boddy2( $title = '', $html = '', $condicion = 0 ){
        echo '<body id="page-top">';

        $this -> wrapper($title, $html, $condicion);

        $this -> logoutModal();

        $this -> includeScripts( $title );

        echo '</body>';
    }

    //Page Wrapper
    public function wrapper( $title = '', $html = '', $condicion = 0 ){
        echo '<div id="wrapper">';
        
            $this -> slidebar();

            $this -> contentwrapper($title, $html, $condicion);

        echo '</div>';
    }

    //Slide bar for the menu
    public function slidebar(){
        include "../Core/View/sidebar.php";         
    }

    //Content Wrapper
    public function contentwrapper( $title = '', $html = '', $condicion = 0 ){
        echo '<div id="content-wrapper" class="d-flex flex-column">';
        
            $this -> maincontent($title, $html, $condicion);

            $this -> footer();

        echo '</div>';
    }

    //Method to manage the main content
    public function maincontent( $title = '', $html = '', $condicion = 0 ){
        echo '<div id="content">';

            $this -> toolbar();

            $this -> pagecontent($title, $html, $condicion);

        echo '</div>';
    }

    //Method for the toolbar
    public function toolbar(){
        echo '<!-- Topbar -->';
        include "../Core/View/topbar.php"; 
      echo '<!-- End of Topbar -->';
    }

    //Begin Page Content
    public function pagecontent( $title = '', $html = '', $condicion = 0 ){

        echo '<div class="container-fluid">';

            $this -> pageheadding($title);
        
            $this -> contenttrow($html, $condicion);

        echo '</div>
        <!-- /.container-fluid -->';
    }

    //Method to display the title of the content
    public function pageheadding( $title ){

        if( empty($title) ){
        
            echo '<!-- Page Heading -->';
            include "../Core/View/pageheading.php";
            echo '<!-- End Page Heading -->'; 
        
        } else {
            
            echo '<!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">'.$title.'</h1>
                      </div>
            <!-- End Page Heading -->';
        
        }

    }

    //Method to display the content inside
    public function contenttrow( $html = '', $condicion = 0 ){

        if( empty($html) ){
        
            include "../Core/View/contentrow.php"; 
        
        } else {
            if($condicion===0){
                //require_once 'app/'.$html.'.html';
                echo $html;
            } else {
                require_once 'app/'.$html.'.php';
            }
            
            
        
        }
    }
    
    //Method for the page footer
    public function footer(){
        include "../Core/View/footer.php";
    }

    //Method to display the logout Modal
    public function pageScroll(){
        echo '<!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
          <i class="fas fa-angle-up"></i>
        </a>';
    }

    //Method to display the logout Modal
    public function logoutModal(){
        include "../Core/View/logoutmodal.php";
    }

    //Including JS Scripts
    public function includeScripts( $title ){

        if( $title === "Welcome"){

            echo '  <!-- Bootstrap core JavaScript-->
                    <!-- Core plugin JavaScript-->
                    <!-- Custom scripts for all pages-->
                    <!-- Page level plugins -->
                    <!-- Page level custom scripts -->';
                    include "../Core/View/scripts.php"; 

        } else {

            echo ' <!-- Bootstrap core JavaScript-->
              <script src="public/vendor/jquery/jquery.min.js"></script>
              <script src="public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

              <!-- JQuery Mask-->
              <script src="public/vendor/jquerymask/dist/jquery.mask.min.js"></script>
              
              <!-- Custom scripts for all pages-->
              <script src="public/js/sb-admin-2.min.js"></script>
              
              <!-- Custom scripts for kendo-->
              <script src="public/vendor/kendo/js/jszip.min.js"></script>
              <script src="public/vendor/kendo/js/kendo.all.min.js"></script>
              
              <!-- FlexDataList JQuery-->
              <script src="public/vendor/flexDataList/jquery.flexdatalist.min.js"></script>
              
              <!-- JQuery Validator-->
              <script src="public/vendor/jquery-validator/jquery.validate.min.js"></script>
              
              ';
            
              
        }
       

        echo '</body>
        </html>';

    }

    public function addJS( $fileName){

        echo '<script language="javascript" type="text/javascript">';
        require_once '../app/js/'.$fileName.'.js';
        echo '</script>';

        echo '</head>';

    }
    

}


?>
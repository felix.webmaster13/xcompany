<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class RolesModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];  
            
            $fields['name']         = $data -> name;
            
            $this -> db -> insertar('fvsoftwa_inventory.roles', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']         = $data -> name;
        
        $this -> db -> actualizar('fvsoftwa_inventory.roles', $fields, 'id_role='.$data -> id_role);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    id_role,
                    name
                FROM 
                    fvsoftwa_inventory.roles ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

    //Method to getApplications for each company
    public function getAllActive(){
        //echo "function to query";
        $sql="  SELECT 
                    id_role,
                    name
                FROM 
                    fvsoftwa_inventory.roles
                WHERE
                    active=1 ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
<?php
namespace app\Models;

use app\Validators\IndexValidator;
use Libraries\DBDriver;

use Exception;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class IndexModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

       // $validation = new IndexValidator( $this, $data );
       // $validation -> valid();

        try {
            
            $validation = new IndexValidator( $this, $data );
            $user_id = $validation -> valid();
            
            //INICIO DE LA SESSION DEL USUARIO
            return $this -> getUserLogin( $user_id );
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
        //echo "Save Method";
    }


    //Method to return if the user and password it is correct
    public function validateUserLogin( $user, $pass ){
        //echo "function to query";
        $sql="  SELECT 
                    user_id
                FROM 
                    fvsoftwa_inventory.usuarios
                WHERE
                    user_name='".$user."' and pass='".$pass."'    ";
        $this -> db -> setQuery($sql);
        return $this -> db -> getObject();

    }

    //Method to get User Information
    public function getUserLogin( $user_id ){
        //echo "function to query";
        $sql="  SELECT 
                    user_id,
                    name,
                    user_name,
                    pass
                FROM 
                    fvsoftwa_inventory.usuarios
                WHERE
                    user_id='".$user_id."' ";
        $this -> db -> setQuery($sql);
        return $this -> db -> getObject();

    }

}

?>
<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class CreateCompaniesModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];  
            
            $fields['name']             = $data -> name;
            $fields['id_serie']         = $data -> id_serie;
            $fields['rnc']              = $data -> rnc;
            $fields['id_cost_method']   = $data -> id_cost_method;
            
            $this -> db -> insertar('fvsoftwa_inventory.company', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']             = $data -> name;
        $fields['id_serie']         = $data -> id_serie;
        $fields['rnc']              = $data -> rnc;
        $fields['id_cost_method']   = $data -> id_cost_method;
        
        $this -> db -> actualizar('fvsoftwa_inventory.company', $fields, 'id_company='.$data -> id_company);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    A.id_company,
                    A.name,
                    A.id_serie,
                    A.rnc,
                    B.name AS serie_name,
                    CASE
                        WHEN id_cost_method = 1
                        THEN
                            'AVERAGE'
                        ELSE
                            CASE
                                WHEN id_cost_method = 2
                                THEN
                                    'FIFO'
                                ELSE
                                CASE
                                    WHEN id_cost_method = 3
                                    THEN
                                        'LIFO'
                                    ELSE
                                        ''
                                END
                            END
                    END as cost_method_name
                FROM 
                    fvsoftwa_inventory.company AS A LEFT JOIN
                    fvsoftwa_accounting.series_ncf AS B
                        ON  A.id_serie=B.id_serie";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

     //Method to getApplications for each company
     public function getById( $company ){
        //echo "function to query";
        $sql="  SELECT 
                    A.id_company,
                    A.name,
                    A.id_serie,
                    B.name AS serie_name,
                    A.rnc
                FROM 
                    fvsoftwa_inventory.company AS A INNER JOIN
                    fvsoftwa_accounting.series_ncf AS B
                        ON  A.id_serie=B.id_serie
                WHERE
                    A.id_company=".$company;
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
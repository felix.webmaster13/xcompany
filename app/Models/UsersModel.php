<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class UsersModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];  
            
            $fields['name']         = $data -> name;
            $fields['user_name']    = $data -> user_name;
            $fields['pass']         = $data -> pass;
            
            $this -> db -> insertar('fvsoftwa_inventory.usuarios', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']         = $data -> name;
        
        $this -> db -> actualizar('fvsoftwa_inventory.usuarios', $fields, 'user_id='.$data -> user_id);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    user_id,
                    name,
                    user_name,
                    pass
                FROM 
                    fvsoftwa_inventory.usuarios ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();
    }

    //Method to getApplications for each company
    public function getAllActive(){
        //echo "function to query";
        $sql="  SELECT 
                    user_id,
                    name,
                    user_name,
                    pass
                FROM 
                    fvsoftwa_inventory.usuarios
                WHERE
                    active=1 ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();
    }

}

?>
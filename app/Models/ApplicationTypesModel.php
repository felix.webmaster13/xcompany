<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class ApplicationTypesModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            $validation = new ArticuloValidator( $this, $data );
            $validation -> valid();

            //print_r($data);
            $fields = [];  
            
            $fields['code']         = $data -> code;
            
            if( $data -> modulesval[$i] === "true" ){
                        
                $sql3=" INSERT INTO fvsoftwa_inventory.users_vs_app (id_user, id_app) VALUES (".$data -> id_user.",".$data -> modules[$i].")";
                //echo $sql3;
                $this -> db -> setQuery($sql3);
                $this -> db -> execute();

            } else {
                
                $sql3=" DELETE FROM fvsoftwa_inventory.users_vs_app WHERE id_user=".$data -> id_user." and id_app=".$data -> modules[$i]." ";
                //echo $sql3;
                $this -> db -> setQuery($sql3);
                $this -> db -> execute();

            }
            
            $this -> db -> insertar('fvsoftwa_inventory.articulos', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        /*try {
            
            $validation = new ArticuloValidator( $this, $data );
            $validation -> valid();
 
            //print_r($data);
        $fields = [];  

        $fields['code']         = $data -> code;
        $fields['name']         = $data -> name;
        $fields['part_no']      = $data -> part_no;
        $fields['id_ubicacion'] = $data -> id_ubicacion;
        $fields['id_unidad']    = $data -> id_unidad;
        $fields['onhand']       = $data -> onhand;
        $fields['barcode']      = $data -> barcode;
        $fields['type_id']      = $data -> type_id;
        $fields['reorden']      = $data -> reorden;
        $fields['minimo']       = $data -> minimo;
        $fields['maximo']       = $data -> maximo;
        $fields['active']       = $data -> active;
        
        $this -> db -> actualizar('articulos', $fields, 'id_articulo='.$data -> id_articulo);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";*/
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_app_type,
                    app.name,
                    CASE
                        WHEN app.active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    app.active as active
                FROM 
                    fvsoftwa_inventory.applications_types AS app ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }


   
    //Method to getApplications for each company
    public function getById( $id_app_type ){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_app_type,
                    app.name,
                    CASE
                        WHEN app.active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    app.active as active
                FROM 
                    fvsoftwa_inventory.applications_types AS app
                WHERE
                    app.id_app_type=".$id_app_type." ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObject();

    }

}

?>
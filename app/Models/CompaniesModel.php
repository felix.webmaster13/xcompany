<?php
namespace app\Models;

use app\Validators\IndexValidator;
use Libraries\DBDriver;
use Exception;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class CompaniesModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to getApplications for each company
    public function getApplicationsByCompany( $data ){
        //echo "function to query";
        //echo $data -> id_user;
        $sql="  SELECT 
                    app.name,
                    app.url 
                FROM 
                    fvsoftwa_inventory.applications AS app INNER JOIN
                    fvsoftwa_inventory.roles_vs_app AS rolapp
                        ON rolapp.id_app=app.id_app INNER JOIN
                    fvsoftwa_inventory.users_vs_role AS usrrole
                        ON usrrole.id_role=rolapp.id_role
                WHERE
                    app.id_company=".$data -> id_company."
                    AND app.id_app_type=".$data -> id_app_type."
                    AND usrrole.id_user=".$data -> id_user."
                    AND app.active=1
                ORDER by
                    app.id_app ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

    //Method to getApplications for each company
    public function getById( $company_id ){
        //echo "function to query";
        $sql="  SELECT 
                    name
                FROM 
                    fvsoftwa_inventory.company
                WHERE
                    id_company=".$company_id." ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObject();


    }


    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    id_company,
                    name
                FROM 
                    fvsoftwa_inventory.company
                ORDER by
                    name ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

    //Method to getApplications for each company
    public function getAllByAccess( $filters ){
        //echo "function to query";
        $sql="  SELECT 
                    comp.id_company,
                    comp.name
                FROM 
                    fvsoftwa_inventory.company AS comp INNER JOIN
                    fvsoftwa_inventory.users_vs_company as usrcomp
                        ON usrcomp.id_company=comp.id_company
                        AND usrcomp.id_user=".$filters -> user_id."
                ORDER by
                    comp.name ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
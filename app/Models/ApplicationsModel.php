<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class ApplicationsModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];  
            
            $fields['name']         = $data -> name;
            $fields['id_company']   = $data -> id_company;
            $fields['id_app_type']  = $data -> id_app_type;
            $fields['url']          = $data -> url;
            $fields['prefijo']      = $data -> prefijo;
            $fields['active']       = $data -> active;
            
            $this -> db -> insertar('fvsoftwa_inventory.applications', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']         = $data -> name;
        $fields['id_company']   = $data -> id_company;
        $fields['id_app_type']  = $data -> id_app_type;
        $fields['url']          = $data -> url;
        $fields['prefijo']      = $data -> prefijo;
        $fields['active']       = $data -> active;
        
        $this -> db -> actualizar('fvsoftwa_inventory.applications', $fields, 'id_app='.$data -> id_app);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_app,
                    app.name,
                    app.id_company as id_company,
                    comp.name as company_name,
                    app.url,
                    app.prefijo,
                    app.id_app_type,
                    apptype.name as id_app_type_name,
                    CASE
                        WHEN app.active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    app.active as active
                FROM 
                    fvsoftwa_inventory.applications AS app INNER JOIN
                    fvsoftwa_inventory.company AS comp
                        ON  comp.id_company=app.id_company INNER JOIN
                    fvsoftwa_inventory.applications_types AS apptype
                        ON  apptype.id_app_type=app.id_app_type
                ORDER BY
                    app.id_app";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();
    }

    //Method to getApplications for each company
    public function getAllActive(){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_app,
                    app.name,
                    app.id_company as id_company,
                    comp.name as company_name,
                    app.url,
                    app.prefijo,
                    app.id_app_type,
                    apptype.name as id_app_type_name,
                    CASE
                        WHEN app.active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    app.active as active
                FROM 
                    fvsoftwa_inventory.applications AS app INNER JOIN
                    fvsoftwa_inventory.company AS comp
                        ON  comp.id_company=app.id_company INNER JOIN
                    fvsoftwa_inventory.applications_types AS apptype
                        ON  apptype.id_app_type=app.id_app_type
                WHERE
                    app.active=1
                ORDER BY
                    app.id_app";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();
    }

    //Method to getApplications for each company
    public function getById( $app_id ){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_app,
                    app.name,
                    app.id_company as id_company,
                    comp.name as company_name,
                    app.url,
                    app.prefijo,
                    app.id_app_type,
                    apptype.name as id_app_type_name,
                    CASE
                        WHEN app.active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    app.active as active
                FROM 
                    fvsoftwa_inventory.applications AS app INNER JOIN
                    fvsoftwa_inventory.company AS comp
                        ON  comp.id_company=app.id_company INNER JOIN
                    fvsoftwa_inventory.applications_types AS apptype
                        ON  apptype.id_app_type=app.id_app_type
                WHERE
                    app.id_app=".$app_id."
                ORDER BY
                    app.id_app";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();
    }

}

?>
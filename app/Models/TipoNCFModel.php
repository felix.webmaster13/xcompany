<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

use app\Models\CreateCompaniesModel;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class TipoNCFModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];  

            $datosCompany = new CreateCompaniesModel();

            $data2 = $datosCompany -> getById(1);
           
            $fields['name']         = $data -> name;
            $fields['id_serie']     = $data2[0]['id_serie'];
            $fields['prefijo']      = $data -> prefijo;

           // print_r($fields);
            
            $this -> db -> insertar('fvsoftwa_accounting.tipo_ncf', $fields);
            $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']         = $data -> name;
        $fields['prefijo']      = $data -> prefijo;
        
        $this -> db -> actualizar('fvsoftwa_accounting.tipo_ncf', $fields, 'id_tipo_ncf='.$data -> id_tipo_ncf);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    A.id_tipo_ncf,
                    A.name,
                    A.id_serie,
                    B.name AS serie_name,
                    A.prefijo
                FROM 
                    fvsoftwa_accounting.tipo_ncf AS A INNER JOIN
                    fvsoftwa_accounting.series_ncf AS B
                        ON A.id_serie=B.id_serie
                GROUP BY
                    A.id_tipo_ncf,
                    A.name,
                    A.id_serie,
                    B.name,
                    A.prefijo";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

    //Method to getApplications for each company
    public function getById($tipo){
        //echo "function to query";
        $sql="  SELECT 
                    A.id_tipo_ncf,
                    A.name,
                    A.id_serie,
                    B.name AS serie_name,
                    A.prefijo
                FROM 
                    fvsoftwa_accounting.tipo_ncf AS A INNER JOIN
                    fvsoftwa_accounting.series_ncf AS B
                        ON A.id_serie=B.id_serie
                WHERE
                    A.id_tipo_ncf=".$tipo."
                GROUP BY
                    A.id_tipo_ncf,
                    A.name,
                    A.id_serie,
                    B.name,
                    A.prefijo";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class UsersInCompanyModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];
            $fields2 = [];
            $role = $data -> id_company;
           
            $datos = $this -> getCheckByRole($data);
           // print_r($datos);
            /*echo count($datos);
            exit;*/
            if( count($datos) === 0){

                //Insetar nuevos permisos
                for($i=0;$i<=count($data -> modules)-1;$i++){
                
                
                    $sql2=" INSERT INTO fvsoftwa_inventory.users_vs_company (id_company, id_user) VALUES (".$data -> id_company.",".$data -> modules[$i].")";
                    //echo $sql2;
                    $this -> db -> setQuery($sql2);
                    $this -> db -> execute();
                
                }

            } else {
                
                //Actualizar nuevos permisos
                for($i=0;$i<=count($data -> modules)-1;$i++){
                    //echo $data -> modulesval[$i];
                    if( $data -> modulesval[$i] === "true" ){
                        
                        $sql3=" INSERT INTO fvsoftwa_inventory.users_vs_company (id_company, id_user) VALUES (".$data -> id_company.",".$data -> modules[$i].")";
                        //echo $sql3;
                        $this -> db -> setQuery($sql3);
                        $this -> db -> execute();

                    } else {
                        
                        $sql3=" DELETE FROM fvsoftwa_inventory.users_vs_company WHERE id_company=".$data -> id_company." and id_user=".$data -> modules[$i]." ";
                        //echo $sql3;
                        $this -> db -> setQuery($sql3);
                        $this -> db -> execute();

                    }
                    
                
                }

            }
            
            
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
       /* $fields = [];  

        $fields['name']         = $data -> name;
        
        $this -> db -> actualizar('fvsoftwa_inventory.roles', $fields, 'id_role='.$data -> id_role);
        $this -> db -> execute();*/
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getUsersByCompany($fields){
        //echo "function to query";
        $sql="  SELECT 
                    cue.user_id,
                    cue.name,
                    appc.id_users_vs_company
                FROM 
                    fvsoftwa_inventory.usuarios AS cue LEFT JOIN
                    fvsoftwa_inventory.users_vs_company as appc
                        ON cue.user_id=appc.id_user
                        AND appc.id_company=".$fields -> id_company."
                WHERE
                    cue.active=1
                GROUP BY
                    cue.user_id";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

    //Method to getApplications for each company
    public function getCheckByRole($fields){
        //echo "function to query";
        $sql="  SELECT 
                    rapp.id_users_vs_company
                FROM 
                    fvsoftwa_inventory.users_vs_company as rapp
                WHERE
                    rapp.id_company=".$fields -> id_company." ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
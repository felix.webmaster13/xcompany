<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class ClientsImportModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
         try {
             
           //print_r($data);
            $fields = [];  
            
            $fields['name']     = $data -> name;
            $fields['rnc']      = $data -> rnc;
            $fields['phone']    = $data -> phone;
            $fields['email']    = $data -> email;
            $fields['address']  = $data -> address;
            
            $this -> db -> insertar('clientes', $fields);
            $this -> db -> execute();
             
         } catch ( Exception $e ){
             //echo $e->getMessage();
             return $e->getMessage();
         }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
         try {
             
           //print_r($data);
            $fields = [];  

            $fields['name']     = $data -> name;
            $fields['rnc']      = $data -> rnc;
            $fields['phone']    = $data -> phone;
            $fields['email']    = $data -> email;
            $fields['address']  = $data -> address;
            $fields['active']   = $data -> active;
            
            $this -> db -> actualizar('clientes', $fields, 'id_cliente='.$data -> id_cliente);
            $this -> db -> execute();
             
         } catch ( Exception $e ){
             //echo $e->getMessage();
             return $e->getMessage();
         }
         //echo "Save Method";
     }
 

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    id_cliente,
                    name,
                    rnc,
                    phone,
                    email,
                    address,
                    CASE
                        WHEN active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    active
                FROM 
                    clientes
                ORDER BY
                    id_cliente";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }


    //Method to getApplications for each company
    public function getAllActive( $articulo ){
        //echo "function to query";
        $sql="  SELECT 
                    id_cliente,
                    name,
                    CASE
                        WHEN active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    active
                FROM 
                    clientes
                WHERE
                    active=1 and name LIKE '%".$articulo."%'
                ORDER BY
                    id_cliente";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
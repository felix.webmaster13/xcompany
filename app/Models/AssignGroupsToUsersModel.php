<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

use app\Validators\ArticuloValidator;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class AssignGroupsToUsersModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/

            //print_r($data);
            $fields = [];
            $fields2 = [];
            $role = $data -> id_user;
           
            

           /* echo count($data -> modules);
            exit;*/
            for($i=0;$i<=count($data -> modules)-1;$i++){
                
                
                    if( $data -> modulesval[$i] === "true" ){
                        
                        $sql3=" INSERT INTO fvsoftwa_inventory.users_vs_role (id_user, id_role) VALUES (".$data -> id_user.",".$data -> modules[$i].")";
                        //echo $sql3;
                        $this -> db -> setQuery($sql3);
                        $this -> db -> execute();

                    } else {
                        
                        $sql3=" DELETE FROM fvsoftwa_inventory.users_vs_role WHERE id_user=".$data -> id_user." and id_role=".$data -> modules[$i]." ";
                        //echo $sql3;
                        $this -> db -> setQuery($sql3);
                        $this -> db -> execute();

                    }
                
            }
            
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        
        try {
            
            /*$validation = new ArticuloValidator( $this, $data );
            $validation -> valid();*/
 
            //print_r($data);
        $fields = [];  

        $fields['name']         = $data -> name;
        
        $this -> db -> actualizar('fvsoftwa_inventory.roles', $fields, 'id_role='.$data -> id_role);
        $this -> db -> execute();
            
        } catch ( Exception $e ){
            //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }

    //Method to getApplications for each company
    public function getRolesByUser($fields){
        //echo "function to query";
        $sql="  SELECT 
                    app.id_role,
                    app.name,
                    rapp.id_users_vs_role
                FROM 
                    fvsoftwa_inventory.roles AS app LEFT JOIN
                    fvsoftwa_inventory.users_vs_role as rapp
                        ON app.id_role=rapp.id_role
                        AND rapp.id_user=".$fields -> user_id."
                WHERE
                    app.active=1
                GROUP BY
                    app.id_role
                ORDER BY
                        app.id_role ";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
<?php
namespace app\Models;

use Libraries\DBDriver;
use Exception;

/*
 * 
 * Class to manipulate the database información
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 14 de Abril del 2020 
 * 
 */

Class ClienteModel{

    protected $db;

    public function __construct(){
        //echo "Controllador IndexModel";
        $this -> db = new DBDriver();
       
    }

    //Method to save the data into a database
    public function save( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        try {
             
           //print_r($data);
            $fields = [];  
            
            if( empty($data -> id_cliente)){

                $fields['name']         = $data -> name;
                $fields['rnc']          = $data -> rnc;
                $fields['phone']        = $data -> phone;
                $fields['email']        = $data -> email;
                $fields['id_tipo_ncf']  = $data -> id_tipo_ncf;
                $fields['active']       = $data -> active;
                
                $this -> db -> insertar('fvsoftwa_inventory.clientes', $fields);
                $this -> db -> execute();
                

            } else {

                $fields['name']         = $data -> name;
                $fields['rnc']          = $data -> rnc;
                $fields['phone']        = $data -> phone;
                $fields['email']        = $data -> email;
                $fields['id_tipo_ncf']  = $data -> id_tipo_ncf;
                $fields['active']       = $data -> active;
                
                $this -> db -> actualizar('fvsoftwa_inventory.clientes', $fields, 'id_cliente='.$data -> id_cliente);
                $this -> db -> execute();
             

            }
             
        } catch ( Exception $e ){
             //echo $e->getMessage();
            return $e->getMessage();
        }
         //echo "Save Method";
     }


    //Method to save the data into a database
    public function update( $data ){

        // $validation = new IndexValidator( $this, $data );
        // $validation -> valid();
 
        /* try {
             
           //print_r($data);
            $fields = [];  

            $fields['name']     = $data -> name;
            $fields['rnc']      = $data -> rnc;
            $fields['phone']    = $data -> phone;
            $fields['email']    = $data -> email;
            $fields['address']  = $data -> address;
            $fields['active']   = $data -> active;
            
            $this -> db -> actualizar('clientes', $fields, 'id_cliente='.$data -> id_cliente);
            $this -> db -> execute();
             
         } catch ( Exception $e ){
             //echo $e->getMessage();
             return $e->getMessage();
         }*/
         //echo "Save Method";
     }
 

    //Method to getApplications for each company
    public function getAll(){
        //echo "function to query";
        $sql="  SELECT 
                    id_cliente,
                    name,
                    rnc,
                    phone,
                    email,
                    id_tipo_ncf,
                    CASE
                        WHEN active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    active
                FROM 
                    fvsoftwa_inventory.clientes
                ORDER BY
                    id_cliente";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }


    //Method to getApplications for each company
    public function getAllActive( $articulo = "" ){
        //echo "function to query";
        $sql="  SELECT 
                    id_cliente,
                    name,
                    rnc,
                    phone,
                    email,
                    id_tipo_ncf,
                    CASE
                        WHEN active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    active
                FROM 
                    fvsoftwa_inventory.clientes
                WHERE
                    active=1
                ORDER BY
                    id_cliente";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }


    //Method to getApplications for each company
    public function getById( $articulo ){
        //echo "function to query";
        $sql="  SELECT 
                    id_cliente,
                    name,
                    rnc,
                    phone,
                    email,
                    id_tipo_ncf,
                    id_cuenta,
                    CASE
                        WHEN active=1
                            THEN
                                'Activo'
                            ELSE 
                                'Inactivo'
                    END as active_name,
                    active
                FROM 
                    fvsoftwa_inventory.clientes
                WHERE
                    id_cliente=".$articulo."
                ORDER BY
                    id_cliente";
                    //echo $sql;
                    //exit;
        $this -> db -> setQuery($sql);
        return $this -> db -> getObjectList();


    }

}

?>
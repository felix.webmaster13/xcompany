$(document).ready(function() {
   
  $("#gridRoles").kendoGrid({
    dataSource: {
        transport: {
            read: {
                url: "?Task=RolesController.getAll",
                type: "POST"
            },
            update: {
                url: "?Task=RolesController.upd",
                type: "POST",
                complete: function(e) {
                    
                    if(e.responseJSON.status == 500){
                        alert("El barcode introducido ya esta siendo utilizado por otro articulo");
                        return false;
                        //$('#ValidationFailModal').modal('Show');
                    } else {
                        $("#gridRoles").data("kendoGrid").dataSource.read(); 
                        var grid = $("#gridRoles").data("kendoGrid");
                        grid.refresh();

                        //window.location ="?Task=ArticuloController";
                    }
                    
                }
            },
            create: {
                url: "?Task=RolesController.add",
                type: "POST",
                complete: function(e) {
                    $("#gridRoles").data("kendoGrid").dataSource.read(); 
                    var grid = $("#gridRoles").data("kendoGrid");
                    grid.refresh();
                 }
            }
        },
        schema: {
            data: "data",
            model: {
                id: "id_role",
                fields: {
                    name: { type:"string", validation: { required: true } }
                }
            }
        },
        pageSize: 20
    },
    height: 450,
    sortable: true,
    noRecords: true,
    filterable: true,
    reordenable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    toolbar: [
        { name:"create", text:"Crear Nuevo" },
        { name:"excel", text:"Exportar a Excel" }
    ],
    excel: {
        fileName: "Compañias.xlsx"
    },
    editable: "inline",
    columns: [{
        field: "id_role",
        title: "Código",
        width: 110,
        hidden: true
    },{
        field: "name",
        title: "Nombre",
        width: 200
    },{ 
        /*command: ["edit", "destroy"], */
        command: [
                    { name:"edit", text: { edit:"Actualizar", update: "Actualizar", cancel: "Cancelar"} }
                ],
        title: "&nbsp;", 
        width: "250px" 
    }]
  });


     
});


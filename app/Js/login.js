$(document).ready(function() {

var frmlogin = $("#frmlogin");

  $.validator.addMethod('strongPassword', function(value, element) {
   return this.optional(element)
    || value.length >= 6
    && /\d/.test(value)
    && /[a-z]/i.test(value); 
  }, 'La contraseña debe de contener por lo menos 6 caracteres y un número');

  frmlogin.validate({
      
    rules: {
      username: 'required',
      password: {
          required: true,
          strongPassword: true
      }
    },
    submitHandler: function(form) {
      //form.submit();
      var username = $("#username").val();
      var password = $("#password").val();

      $.ajax({
        method: "POST",
        url: "?Task=indexController.save",
        data: { user: username, pass: password },
        dataType: 'JSON',
        success: function(response){
          
          window.location ="?Task=CompaniesController";
        
        },
        error: function(response){
         
          $('#ValidFailModal').modal('show');
          $("#frmlogin")[0].reset();
        }
        //context: document.body
    }).done(function() {
        $( this ).addClass( "done" );
       // $.alert("Done");
    });

      return false;

    }

  });


   


});


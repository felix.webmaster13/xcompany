$(document).ready(function() {
   
  $("#gridUsers").kendoGrid({
    dataSource: {
        transport: {
            read: {
                url: "?Task=UsersController.getAll",
                type: "POST"
            },
            update: {
                url: "?Task=UsersController.upd",
                type: "POST",
                complete: function(e) {
                    
                    if(e.responseJSON.status == 500){
                        alert("El barcode introducido ya esta siendo utilizado por otro articulo");
                        return false;
                        //$('#ValidationFailModal').modal('Show');
                    } else {
                        $("#gridUsers").data("kendoGrid").dataSource.read(); 
                        var grid = $("#gridUsers").data("kendoGrid");
                        grid.refresh();

                        //window.location ="?Task=ArticuloController";
                    }
                    
                }
            },
            create: {
                url: "?Task=UsersController.add",
                type: "POST",
                complete: function(e) {
                    $("#gridUsers").data("kendoGrid").dataSource.read(); 
                    var grid = $("#gridUsers").data("kendoGrid");
                    grid.refresh();
                 }
            }
        },
        schema: {
            data: "data",
            model: {
                id: "user_id",
                fields: {
                    user_id: { type:"numbber", editable: false, validation: { required: true } },
                    name: { type:"string", editable: true, validation: { required: true } },
                    user_name: { type:"string", editable: true, validation: { required: true } },
                    pass: { type:"string", editable: true, validation: { required: true } }
                }
            }
        },
        pageSize: 20
    },
    height: 450,
    sortable: true,
    noRecords: true,
    filterable: true,
    reordenable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    toolbar: [
        { name:"create", text:"Crear Nuevo" },
        { name:"excel", text:"Exportar a Excel" }
    ],
    excel: {
        fileName: "Usuarios.xlsx"
    },
    editable: "popup",
    edit: function (e) {
        $(e.container).parent().css({
            width: 500,
            height: 400
        });
    },
    columns: [{
        field: "user_id",
        title: "Código",
        width: 110,
        hidden: true
    },{
        field: "name",
        title: "Nombre",
        width: 200
    },{
        field: "user_name",
        title: "Usuario",
        width: 200
    },{
        field: "pass",
        title: "Clave",
        width: 200,
        template: '#if(data.pass == null){# <span></span> #}else{# #= "●".repeat(data.pass.length)#  #}#',
        hidden: true
    },{
        field: "passConfirm",
        title: "Confirmar Clave",
        width: 200,
        template: '#if(data.passConfirm == null){# <span></span> #}else{# #= "●".repeat(data.passConfirm.length)#  #}#',
        hidden: true
    }/*,{ 
        command: ["edit"],
        title: "&nbsp;", 
        width: "250px" 
    }*/]
  });


     
});


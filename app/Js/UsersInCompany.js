$(document).ready(function() {
   
    $( "#showSelection" ).prop( "disabled", true );

    // create ComboBox from supplier select HTML element
    $("#cmbCompany").kendoComboBox({
        dataTextField: "name",
        dataValueField: "id_company",
        filter: "contains",
        autoWidth: true,
        dataSource: {
            transport: {
                read: {
                    url: "?Task=UsersInCompanyController.getCompanies",
                    type: "post"
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id_company",
                    fields: {
                        id_company: { type: "number", editable: false, nullable: true },
                        name: { type: "string", editable: true, nullable: true }
                    }
                }
            }
        }
    });



    //ComboBox Styles
    $("[role=combobox]").removeClass("k-input");
    $("[role=combobox]").addClass("selectpicker");
    $("[role=combobox]").addClass("k-invalid");

    //When click on Combobox display the list values
    $( "[aria-owns=cmbCompany_listbox]" ).on( "click", function(){
    
        $( "[aria-controls=cmbCompany_listbox]" ).trigger( "click" ); 
    
    });


    $( "#btnBuscar" ).on("click", function() {

        $( "#showSelection" ).prop( "disabled", false );

        var app = $("#cmbCompany").val();
        //alert(role);
        if(app === null){
            $('#ValidationFailModal').modal('show');
            return false;
        }

        $("#cmbCompany").data("kendoComboBox").enable(false);
 

        var grid = $("#gridAsignarUsuarios").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: "?Task=UsersInCompanyController.getUsersByCompany",
                        type: "POST",
                        data: {
                            id_company: app
                        }
                    }
                },
                schema: {
                    data: "data",
                    model: {
                        id: "user_id",
                        fields: {
                            user_id: { type:"number", editable: false },
                            id_app2: { type:"number", editable: false },
                            name: { type:"string", editable: false },
                            id_users_vs_company: { type:"string", editable: false }
                        }
                    }
                }
            },
            height: 450,
            noRecords: true,
            selectable: true,
            filterable: {
                mode: "row"
            },
            pageable: {
                refresh: true
            },
            excel: {
                fileName: "Usuarios.xlsx"
            },
            columns: [{
                field: "user_id",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field:"id_app2", 
                title: "Seleccionar",
                template: '<input type="checkbox" #= id_users_vs_company>0 ? \'checked="checked"\' : "" # class="checkbox row-checkbox #= id_users_vs_company>0 ? "k-state-selected" : "" #" />',
                headerTemplate: '<input type="checkbox" id="header-chb" class="checkbox" />',
                width: 80,
                attributes:{style:"text-align:center;"},
                headerAttributes:{style:"text-align:center;"}
            },{
                field: "id_users_vs_company",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field: "name",
                title: "Nombre",
                width: 500,
                filterable: {
                    cell: {
                      operator: "contains",
                      suggestionOperator: "contains"
                    }
                  }
            }
        ]

        
        }).data("kendoGrid");

        $("#gridAsignarUsuarios .k-grid-content").on("change", "input.chkbx", function(e) {
            var grid = $("#gridAsignarUsuarios").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
    
            dataItem.set("id_app2", this.checked);
          });

        

        //bind click event to the checkbox
        grid.table.on("click", ".checkbox" , selectRow);


        $('#header-chb').change(function (ev) {
            //alert("here");
            var checked = ev.target.checked;
            //console.log(checked);
            /*var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#gridAsignarUsuarios").data("kendoGrid"),
            dataItem = grid.dataItem(row);*/
            if (checked) {

                $('.row-checkbox').each(function (idx, item) {
                    //console.log($(item).closest('tr').prevObject[0].checked);
                    //console.log(($(item).closest('tr').attr("checked")));
                    
                    if (! ($(item).closest('tr').prevObject[0].checked) ) {
                        // ...
                        $(item).click();
                    }
                    
                });

            }  else  {

                $('.row-checkbox').each(function (idx, item) {
                    //console.log($(item).closest('tr').prevObject[0].checked);
                    //console.log(($(item).closest('tr').attr("checked")));
                    
                    if (($(item).closest('tr').prevObject[0].checked) ) {
                        // ...
                        $(item).click();
                    }
                    
                });

            }  
        });


        $("#showSelection").bind("click", function () {
            
            $('#SaveValidationModal').modal('show');

        });

        $('#btnYes').click(function() {

            var app = $("#cmbCompany").val();
            
            var checked = [];
            var checkedval = [];
            for(var i in checkedIds){
                //alert(checkedIds[i]);
                /*if(checkedIds[i]){
                    checked.push(i);
                }*/
                checked.push(i);
                checkedval.push(checkedIds[i]);
            }
            //alert(checked);
            
            //Enviar Datos a Guardar
            $.ajax({
                method: "POST",
                url: "?Task=UsersInCompanyController.add",
                data: { 
                    id_company: app,
                    modules: checked,
                    modulesval: checkedval
                },
                dataType: 'JSON',
                success: function(response){
                
                    window.location ="?Task=UsersInCompanyController";
                
                },
                error: function(response){
                
                    /*$('#ValidFailModal').modal('show');
                    $("#frmlogin")[0].reset();*/
                }
                //context: document.body
            }).done(function() {
                $( this ).addClass( "done" );
            // $.alert("Done");
            });

        });

        $('#btnNo').click(function() {
            // handle redirect here
            $('#SaveValidationModal').modal('hide');
        });

    });

    
 var checkedIds = {};

 //on click of the checkbox:
 function selectRow() {
     var checked = this.checked,
     row = $(this).closest("tr"),
     grid = $("#gridAsignarUsuarios").data("kendoGrid"),
     dataItem = grid.dataItem(row);

     checkedIds[dataItem.id] = checked;
     //alert(Object.keys(checkedIds));
     if (checked) {
         //alert("Marked");
         //-select the row
         row.addClass("k-state-selected");
         } else {
           /// alert("UnMarked");
         //-remove selection
         row.removeClass("k-state-selected");
     }

 }


    $( "#btnCancel" ).on("click", function() {
        window.location ="?Task=UsersInCompanyController";
    });

});


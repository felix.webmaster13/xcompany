$(document).ready(function() {

    

    // create ComboBox from Items Units select HTML element
    $("#cmbactive").kendoComboBox({

        dataTextField: "Name",
        dataValueField: "Id",
        filter: "contains",
        autoWidth: true,
        dataSource: [
            { Name: "Active", Id: 1 },
            { Name: "Inactive", Id: 0 }
        ]
    });

    // create ComboBox from Items Units select HTML element
    $("#cmbTipoNCF").kendoComboBox({
        dataTextField: "name",
        dataValueField: "id_tipo_ncf",
        filter: "contains",
        autoWidth: true,
        template: '#:data.name#' +
            '(#:data.serie_name##:data.prefijo#)',
        dataSource: {
            transport: {
                read: {
                    url: "?Task=ClienteController.getTipoNCF",
                    type: "post"
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id_tipo_ncf",
                    fields: {
                        id_tipo_ncf: { type: "number", editable: false, nullable: true },
                        name: { type: "string", editable: true, nullable: true },
                        serie_name: { editable: false, nullable: true },
                        prefijo: { editable: false, nullable: true }
                    }
                }
            }
        }
    });

    //ComboBox Styles
    $("[role=combobox]").removeClass("k-input");
    $("[role=combobox]").addClass("selectpicker");
    $("[role=combobox]").addClass("k-invalid");

    //When click on Combobox display the list values
    $( "[aria-owns=cmbactive_listbox]" ).on( "click", function(){
        //alert("Here");
        $( "[aria-controls=cmbactive_listbox]" ).trigger( "click" ); 
    
    });

    $( "[aria-owns=cmbTipoNCF_listbox]" ).on( "click", function(){
        //alert("Here");
        $( "[aria-controls=cmbTipoNCF_listbox]" ).trigger( "click" ); 
    
    });

    //Mask to phones values
    $( "#txtphone" ).on( "keyup", function(){
        
        $("#txtphone").mask('999-999-9999')

    });


     //search for code
     $("#txtname").kendoAutoComplete({
        dataTextField: "name",
        dataValueField: "id_cliente",
        filter: "contains",
        autoWidth: true,
        template: '<span class="k-state-default")">#:data.id_cliente#</span>&nbsp;-&nbsp; ' +
        '<span class="k-state-default")">#:data.name#</span>',
        select: function (e) {
            //alert(e.dataItem.id_articulo);
            //console.log(e.dataItem);
            //Autofill form base on specific item
            $.ajax({
                method: "POST",
                url: "?Task=ClienteController.getById",
                data: { 
                    id_articulo: e.dataItem.id_cliente
                },
                dataType: 'JSON',
                success: function(response){
                
                    //alert(response.data[0].rnc);
                    //window.location ="?Task=ArticuloController";

                    $("#txtid_cliente").val(response.data[0].id_cliente);
                    $("#txtrnc").val(response.data[0].rnc);
                    $("#txtphone").val(response.data[0].phone);
                    $("#txtemail").val(response.data[0].email);
                    $("#txtAddress").val(response.data[0].address);
                    
                    $('#cmbTipoNCF').data('kendoComboBox').value(response.data[0].id_tipo_ncf);
                    $('#cmbactive').data('kendoComboBox').value(response.data[0].active);


                    show_directions();
                
                },
                error: function(response){
                
                   // $('#ValidFailModal').modal('show');
                   
                }
                //context: document.body
            }).done(function() {
                $( this ).addClass( "done" );
            // $.alert("Done");
            });

        },
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    url: "?Task=ClienteController.getAll",
                    type: "post",
                    data: {
                        keyword: function(){
                         return $("#txtname").data("kendoAutoComplete").value();
                        }
                    }
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id_cliente",
                    fields: {
                        id_cliente: { type: "number", editable: false, nullable: true },
                        name: { type: "string", editable: true, nullable: true }
                    }
                }
            }
        },
        placeholder: "Elegir un Articulo..."
    });



    //Disabling Field by Default
    $( "#txtname" ).data("kendoAutoComplete").enable(false);
    $("#txtrnc").prop( "disabled", true );
    $( "#txtphone" ).prop( "disabled", true );
    $( "#txtemail" ).prop( "disabled", true );
    $( "#txtAddress" ).prop( "disabled", true );

    $("#cmbTipoNCF").data("kendoComboBox").enable(false);
    $("#cmbactive").data("kendoComboBox").enable(false);

    $("#btnCancelar").prop( "disabled", true );
    $("#btnProcesar").prop( "disabled", true );

    // New Botton
    $( "#btnNuevo" ).on( "click", function(){

         //Enabling Field by Default
            $( "#txtname" ).data("kendoAutoComplete").enable(true);
            $( "#txtrnc" ).prop( "disabled", false );
            $( "#txtphone" ).prop( "disabled", false );
            $( "#txtemail" ).prop( "disabled", false );
            $( "#txtAddress" ).prop( "disabled", false );
            
            $("#cmbTipoNCF").data("kendoComboBox").enable(true);
            $("#cmbactive").data("kendoComboBox").enable(true);

            $("#btnCancelar").prop( "disabled", false );
            $("#btnProcesar").prop( "disabled", false );

            $("#btnNuevo").prop( "disabled", true );
            $("#btnActualizar").prop( "disabled", true );

            $( "#txtname" ).focus();

            $('#cmbactive').data('kendoComboBox').value(1);


    });

    // Update Botton
    $( "#btnActualizar" ).on( "click", function(){

        //Enabling Field by Default
        $( "#txtname" ).data("kendoAutoComplete").enable(true);
        $( "#txtrnc" ).prop( "disabled", false );
        $( "#txtphone" ).prop( "disabled", false );
        $( "#txtemail" ).prop( "disabled", false );
        $( "#txtAddress" ).prop( "disabled", false );
        
        $("#cmbTipoNCF").data("kendoComboBox").enable(true);
        $("#cmbactive").data("kendoComboBox").enable(true);
        

           $("#btnCancelar").prop( "disabled", false );
           $("#btnProcesar").prop( "disabled", false );

           $("#btnNuevo").prop( "disabled", true );
           $("#btnActualizar").prop( "disabled", true );

           $( "#txtname" ).focus();

   });

     // Call to save form
     $( "#btnProcesar" ).on( "click", function(){

        //Description Validation
        if($("#txtname").val() === ""){
            //alert("Esta vacio");
            
            $("#txtname").removeClass('k-input').parent().removeClass("k-widget k-autocomplete k-header");

            $("#txtname").removeClass("k-invalid").parent().removeClass("k-autocomplete-clearable k-state-default");
            $("[role=presentation]").removeClass("form-control");
            $("#txtname").addClass("is-invalid");

            return false;

        } else {
            //alert("Campo completado");
            $("#txtname").removeClass('k-input').parent().removeClass("k-widget k-autocomplete k-header");

            $("#txtname").removeClass("k-invalid").parent().removeClass("k-autocomplete-clearable k-state-default");
            $("[role=presentation]").removeClass("form-control");
            
            $("#txtname").removeClass("is-invalid");
            $("#txtname").addClass("is-valid");
        }
        

        //Description Validation
        if($("#txtrnc").val() === ""){

            $("#txtrnc").removeClass("k-invalid");
            $("#txtrnc").addClass("is-invalid");

            return false;

        } else {

            $("#txtrnc").removeClass("is-invalid");
            $("#txtrnc").addClass("is-valid");
        }


        //Description Validation
        if($("#txtphone").val() === ""){

            $("#txtphone").removeClass("k-invalid");
            $("#txtphone").addClass("is-invalid");

            return false;

        } else {

            $("#txtphone").removeClass("is-invalid");
            $("#txtphone").addClass("is-valid");
        }

        //Description Validation
        if($("#txtemail").val() === ""){

            $("#txtemail").removeClass("k-invalid");
            $("#txtemail").addClass("is-invalid");

            return false;

        } else {

            $("#txtemail").removeClass("is-invalid");
            $("#txtemail").addClass("is-valid");
        }

        //Description Validation
        if($("#txtAddress").val() === ""){

            $("#txtAddress").removeClass("k-invalid");
            $("#txtAddress").addClass("is-invalid");

            return false;

        } else {

            $("#txtAddress").removeClass("is-invalid");
            $("#txtAddress").addClass("is-valid");
        }

        //cmbtipoNCF Validation
        if($("[aria-owns=cmbTipoNCF_listbox]").val() === ""){
            
            $("[aria-owns=cmbTipoNCF_listbox]").removeClass("dropdown-validation-pass");
            $("[aria-owns=cmbTipoNCF_listbox]").addClass("dropdown-validation-error");
            
            return false;

        } else {

            $("[aria-owns=cmbTipoNCF_listbox]").removeClass("dropdown-validation-error");
            $("[aria-owns=cmbTipoNCF_listbox]").addClass("dropdown-validation-pass");
        }


         //cmbactive Validation
         if($("[aria-owns=cmbactive_listbox]").val() === ""){
            
            $("[aria-owns=cmbactive_listbox]").removeClass("dropdown-validation-pass");
            $("[aria-owns=cmbactive_listbox]").addClass("dropdown-validation-error");
            
            return false;

        } else {

            $("[aria-owns=cmbactive_listbox]").removeClass("dropdown-validation-error");
            $("[aria-owns=cmbactive_listbox]").addClass("dropdown-validation-pass");
        }



        $('#ArticulosModal').modal('show');


        // Start Getting button pressed in the dialog box
        $('#btnYes').click(function() {


            var name    = $("#txtname").val(),
            rnc         = $("#txtrnc").val(),
            phone       = $("#txtphone").val(),
            email       = $("#txtemail").val(),
            address     = $("#txtAddress").val(),
            tipo        = $("#cmbTipoNCF").data('kendoComboBox').value(),
            active      = $("#cmbactive").data('kendoComboBox').value(),
            id_cliente  = $("#txtid_cliente").val();


            $.ajax({
                method: "POST",
                url: "?Task=ClienteController.add",
                data: { 
                    name: name, 
                    rnc: rnc,
                    phone: phone,
                    email: email,
                    address: address,
                    id_tipo_ncf: tipo,
                    active: active,
                    id_cliente: id_cliente
                },
                dataType: 'JSON',
                success: function(response){
                
                    //alert(response.data.status);
                    window.location ="?Task=ClienteController";
                
                },
                error: function(response){
                    //alert(Object.keys(response));
                    //alert(response.status);
                    if( response.status != 200){
                        $('#ValidFailModal').modal('show');
                    } else {
                      //  window.location ="?Task=ClienteController";
                    }
                    
                    //$("#frmlogin")[0].reset();*/
                }
                //context: document.body
            }).done(function() {
                $( this ).addClass( "done" );
            // $.alert("Done");
            });

        });


    });


function show_directions () {

    $("#gridClientDirections").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "?Task=ClientDirectionsController.getById",
                    type: "POST",
                    data:{
                        id_cliente: $("#txtid_cliente").val()
                    }
                },
                update: {
                    url: "?Task=ClientDirectionsController.upd",
                    type: "POST",
                    data:{
                        id_cliente: $("#txtid_cliente").val()
                    },
                    complete: function(e) {
                        
                        /*if(e.responseJSON.status == 500){
                            alert("El barcode introducido ya esta siendo utilizado por otro articulo");
                            return false;
                            //$('#ValidationFailModal').modal('Show');
                        } else {
                            $("#gridClientDirections").data("kendoGrid").dataSource.read(); 
                            var grid = $("#gridApplications").data("kendoGrid");
                            grid.refresh();
    
                            //window.location ="?Task=ArticuloController";
                        }*/
                        
                    }
                },
                create: {
                    url: "?Task=ClientDirectionsController.add",
                    type: "POST",
                    data:{
                        id_cliente: $("#txtid_cliente").val()
                    },
                    complete: function(e) {
                        $("#gridClientDirections").data("kendoGrid").dataSource.read(); 
                        var grid = $("#gridClientDirections").data("kendoGrid");
                        grid.refresh();
                     }
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id_clientes_direcciones",
                    fields: {
                        address: { type:"string", validation: { required: true } }
                    }
                }
            },
            pageSize: 100
        },
        height: 550,
        sortable: true,
        noRecords: true,
        filterable: true,
        reordenable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        toolbar: [
            { name:"create", text:"Crear Nuevo" },
            { name:"excel", text:"Exportar a Excel" }
        ],
        excel: {
            fileName: "ClientsDirections.xlsx"
        },
        editable: "inline",
        columns: [{
            field: "id_clientes_direcciones",
            title: "Código",
            width: 110,
            hidden: true
        },{
            field: "address",
            title: "Direccion",
            width: 200
        },{ 
            /*command: ["edit", "destroy"], */
            command: [
                        { name:"edit", text: { edit:"Actualizar", update: "Actualizar", cancel: "Cancelar"} }
                    ],
            title: "&nbsp;", 
            width: "250px" 
        }]
      });


}
    



    // Call to Cancel form
   $( "#btnCancelar" ).on( "click", function(){

    if(confirm("¿Are you Sure to cancel?")){
        window.location ="?Task=ClienteController";
    } else {
        return false;
    }
    

  });


});


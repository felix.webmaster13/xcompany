$(document).ready(function() {
   
    $( "#showSelection" ).prop( "disabled", true );

    // create ComboBox from supplier select HTML element
    $("#cmbRole").kendoComboBox({
        dataTextField: "name",
        dataValueField: "id_role",
        filter: "contains",
        autoWidth: true,
        dataSource: {
            transport: {
                read: {
                    url: "?Task=PermmisionsRolesController.getRoles",
                    type: "post"
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id_role",
                    fields: {
                        id_role: { type: "number", editable: false, nullable: true },
                        name: { type: "string", editable: true, nullable: true }
                    }
                }
            }
        }
    });

     //ComboBox Styles
     $("[role=combobox]").removeClass("k-input");
     $("[role=combobox]").addClass("selectpicker");
     $("[role=combobox]").addClass("k-invalid");
 
     //When click on Combobox display the list values
     $( "[aria-owns=cmbRole_listbox]" ).on( "click", function(){
     
         $( "[aria-controls=cmbRole_listbox]" ).trigger( "click" ); 
     
     });

    $( "#btnBuscar" ).on("click", function() {

        $( "#showSelection" ).prop( "disabled", false );

        var role = $("#cmbRole").val();
        //alert(role);
        if(role === null){
            $('#ValidationFailModal').modal('show');
            return false;
        }

        $("#cmbRole").data("kendoComboBox").enable(false);


        function dirtyField(data, fieldName){
            var hasClass = $("[data-uid=" + data.uid + "]").find(".k-dirty-cell").length < 1;
            if(data.dirty && data.dirtyFields[fieldName] && hasClass){
              return "<span class='k-dirty'></span>"
            }
            else{
              return "";
            }
          }

        var grid = $("#gridPermmisionsRoles").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: "?Task=PermmisionsRolesController.getPermisionsByRole",
                        type: "POST",
                        data: {
                            id_role: role
                        }
                    }
                },
                schema: {
                    data: "data",
                    model: {
                        id: "id_app",
                        fields: {
                            id_app: { type:"number", editable: false },
                            id_app2: { type:"number", editable: false },
                            name: { type:"string", editable: false },
                            app_type_name: { type:"string", editable: false }
                        }
                    }
                },
                group: {
                    field: "app_type_name", aggregates: [
                       { field: "id_app", aggregate: "count" }
                    ]
                  },
                sort: { 
                    field: "id_app_type", 
                    dir: "ASC" 
                },
                aggregate: [ 
                    { field: "id_app", aggregate: "count" }
                ]
            },
            height: 450,
            noRecords: true,
            selectable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            excel: {
                fileName: "Aplicaciones.xlsx"
            },
            columns: [{
                field: "id_app",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field:"id_app2", 
                title: "Select",
                template: '<input type="checkbox" #= id_roles_vs_app>0 ? \'checked="checked"\' : "" # class="checkbox" />',
                width: 80
            },{
                field: "id_roles_vs_app",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field: "name",
                title: "Nombre",
                width: 500
            },{
                field: "app_type_name",
                title: "App Type",
                width: 500,
                hidden: true
            },{
                field: "id_app_type",
                title: "App Type",
                width: 500,
                hidden: true
            }
        ]

        
        }).data("kendoGrid");

        $("#gridPermmisionsRoles .k-grid-content").on("change", "input.chkbx", function(e) {
            var grid = $("#gridPermmisionsRoles").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
    
            dataItem.set("id_app2", this.checked);
          });

        

        //bind click event to the checkbox
        grid.table.on("click", ".checkbox" , selectRow);

        $("#showSelection").bind("click", function () {
            
            $('#SaveValidationModal').modal('show');

        });

        $('#btnYes').click(function() {

            var role = $("#cmbRole").val();
            
            var checked = [];
            var checkedval = [];
            for(var i in checkedIds){
                //alert(checkedIds[i]);
                /*if(checkedIds[i]){
                    checked.push(i);
                }*/
                checked.push(i);
                checkedval.push(checkedIds[i]);
            }
            //alert(checked);
            
            //Enviar Datos a Guardar
            $.ajax({
                method: "POST",
                url: "?Task=PermmisionsRolesController.add",
                data: { 
                    id_role: role,
                    modules: checked,
                    modulesval: checkedval
                },
                dataType: 'JSON',
                success: function(response){
                
                    window.location ="?Task=PermmisionsRolesController";
                
                },
                error: function(response){
                
                    /*$('#ValidFailModal').modal('show');
                    $("#frmlogin")[0].reset();*/
                }
                //context: document.body
            }).done(function() {
                $( this ).addClass( "done" );
            // $.alert("Done");
            });

        });

        $('#btnNo').click(function() {
            // handle redirect here
            $('#SaveValidationModal').modal('hide');
        });

    });

    
 var checkedIds = {};

 //on click of the checkbox:
 function selectRow() {
     var checked = this.checked,
     row = $(this).closest("tr"),
     grid = $("#gridPermmisionsRoles").data("kendoGrid"),
     dataItem = grid.dataItem(row);

     checkedIds[dataItem.id] = checked;
     //alert(Object.keys(checkedIds));
     if (checked) {
         //alert("Marked");
         //-select the row
         row.addClass("k-state-selected");
         } else {
           /// alert("UnMarked");
         //-remove selection
         row.removeClass("k-state-selected");
     }

 }


    $( "#btnCancel" ).on("click", function() {
        window.location ="?Task=PermmisionsRolesController";
    });

});


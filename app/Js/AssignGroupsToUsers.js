$(document).ready(function() {
   
    $( "#showSelection" ).prop( "disabled", true );

    // create ComboBox from supplier select HTML element
    $("#cmbUsers").kendoComboBox({
        dataTextField: "name",
        dataValueField: "user_id",
        filter: "contains",
        autoWidth: true,
        dataSource: {
            transport: {
                read: {
                    url: "?Task=UsersPermmisionsController.getUsers",
                    type: "post"
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "user_id",
                    fields: {
                        user_id: { type: "number", editable: false, nullable: true },
                        name: { type: "string", editable: true, nullable: true },
                        user_name: { type: "string", editable: true, nullable: true }
                    }
                }
            }
        }
    });


    //ComboBox Styles
    $("[role=combobox]").removeClass("k-input");
    $("[role=combobox]").addClass("selectpicker");
    $("[role=combobox]").addClass("k-invalid");

    //When click on Combobox display the list values
    $( "[aria-owns=cmbUsers_listbox]" ).on( "click", function(){
    
        $( "[aria-controls=cmbUsers_listbox]" ).trigger( "click" ); 
    
    });

    $( "#btnBuscar" ).on("click", function() {

        $( "#showSelection" ).prop( "disabled", false );

        var user = $("#cmbUsers").val();
        //alert(role);
        if(user === null){
            $('#ValidationFailModal').modal('show');
            return false;
        }

        $("#cmbUsers").data("kendoComboBox").enable(false);

        var grid = $("#gridAssignGroupsToUsers").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: "?Task=AssignGroupsToUsersController.getRolesByUser",
                        type: "POST",
                        data: {
                            user_id: user
                        }
                    }
                },
                schema: {
                    data: "data",
                    model: {
                        id: "id_role",
                        fields: {
                            id_role: { type:"number", editable: false },
                            id_role2: { type:"number", editable: false },
                            name: { type:"string", editable: false }
                        }
                    }
                }
            },
            height: 450,
            noRecords: true,
            selectable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            excel: {
                fileName: "Aplicaciones.xlsx"
            },
            columns: [{
                field: "id_role",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field:"id_role2", 
                title: "Select",
                template: '<input type="checkbox" #= id_users_vs_role>0 ? \'checked="checked"\' : "" # class="checkbox" />',
                width: 80
            },{
                field: "id_users_vs_role",
                title: "Código",
                width: 110,
                hidden: true
            },{
                field: "name",
                title: "Nombre",
                width: 500
            }
        ]

        
        }).data("kendoGrid");

       
        //bind click event to the checkbox
        grid.table.on("click", ".checkbox" , selectRow);

        $("#showSelection").bind("click", function () {
            
            $('#SaveValidationModal').modal('show');

        });

        $('#btnYes').click(function() {

            var user = $("#cmbUsers").val();
            
            var checked = [];
            var checkedval = [];
            for(var i in checkedIds){
                /*if(checkedIds[i]){
                    checked.push(i);
                }*/
                checked.push(i);
                checkedval.push(checkedIds[i]);
            }
            //alert(checked);
            
            //Enviar Datos a Guardar
            $.ajax({
                method: "POST",
                url: "?Task=AssignGroupsToUsersController.add",
                data: { 
                    id_user: user,
                    modules: checked,
                    modulesval: checkedval
                },
                dataType: 'JSON',
                success: function(response){
                
                    window.location ="?Task=AssignGroupsToUsersController";
                
                },
                error: function(response){
                
                    /*$('#ValidFailModal').modal('show');
                    $("#frmlogin")[0].reset();*/
                }
                //context: document.body
            }).done(function() {
                $( this ).addClass( "done" );
            // $.alert("Done");
            });

        });

        $('#btnNo').click(function() {
            // handle redirect here
            $('#SaveValidationModal').modal('hide');
        });

    });

    
 var checkedIds = {};

 //on click of the checkbox:
 function selectRow() {
     var checked = this.checked,
     row = $(this).closest("tr"),
     grid = $("#gridAssignGroupsToUsers").data("kendoGrid"),
     dataItem = grid.dataItem(row);

     checkedIds[dataItem.id] = checked;
     //alert(Object.keys(checkedIds)+ '-' +checked);
     if (checked) {
         //-select the row
         row.addClass("k-state-selected");
         } else {
         //-remove selection
         row.removeClass("k-state-selected");
     }

 }


    $( "#btnCancel" ).on("click", function() {
        window.location ="?Task=AssignGroupsToUsersController";
    });

});


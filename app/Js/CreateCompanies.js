$(document).ready(function() {
   
  $("#gridCompanies").kendoGrid({
    dataSource: {
        transport: {
            read: {
                url: "?Task=CreateCompaniesController.getAll",
                type: "POST"
            },
            update: {
                url: "?Task=CreateCompaniesController.upd",
                type: "POST",
                complete: function(e) {
                    
                    
                        $("#gridCompanies").data("kendoGrid").dataSource.read(); 
                        var grid = $("#gridCompanies").data("kendoGrid");
                        grid.refresh();

                   
                    
                }
            },
            create: {
                url: "?Task=CreateCompaniesController.add",
                type: "POST",
                complete: function(e) {
                    $("#gridCompanies").data("kendoGrid").dataSource.read(); 
                    var grid = $("#gridCompanies").data("kendoGrid");
                    grid.refresh();
                 }
            }
        },
        schema: {
            data: "data",
            model: {
                id: "id_company",
                fields: {
                    name: { type:"string", validation: { required: true } },
                    rnc: { type:"number", validation: { required: true } },
                    id_serie: { type:"string", validation: { required: true } },
                    serie_name: { type:"string" },
                    id_cost_method: { type:"string", validation: { required: true } },
                    cost_method_name: { type:"string" }
                }
            }
        },
        pageSize: 20
    },
    height: 450,
    sortable: true,
    noRecords: true,
    filterable: true,
    reordenable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    toolbar: [
        { name:"create", text:"Crear Nuevo" },
        { name:"excel", text:"Exportar a Excel" }
    ],
    excel: {
        fileName: "Compañias.xlsx"
    },
    editable: "inline",
    columns: [{
        field: "id_company",
        title: "Código",
        width: 110,
        hidden: true
    },{
        field: "name",
        title: "Nombre",
        width: 200
    },{
        field: "rnc",
        title: "RNC",
        width: 110
    },{
        field: "id_serie",
        title: "Serie",
        width: 200,
        editor: serieDropDownEditor,
        template: "# if(serie_name != null) {# #=serie_name# #}#"
    },{
        field: "serie_name",
        title: "serie_name",
        width: 200,
        hidden: true 
    },{
        field: "id_cost_method",
        title: "Método de Costeo",
        width: 200,
        editor: costMethodDropDownEditor,
        template: "# if(cost_method_name != null) {# #=cost_method_name# #}#"
    },{
        field: "cost_method_name",
        title: "serie_name",
        width: 200,
        hidden: true
    },{ 
        /*command: ["edit", "destroy"], */
        command: [
                    { name: "edit", text: { edit:"Actualizar", update: "Actualizar", cancel: "Cancelar"} }
                ],
        title: "&nbsp;", 
        width: "250px" 
    }]
  });


  //Method to include DropDownList for the Locations
  function serieDropDownEditor(container, options) {
    //alert(options.field);
    $('<input data-text-field="name" data-value-field="id_serie" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            dataTextField: "name",
            dataValueField: "id_serie",
            value: options.model.id_serie,
            dataSource: {
                transport: {
                    read: "?Task=CreateCompaniesController.getSeries",
                },
                schema: {
                    data: "data",
                    model: {
                        id: "id_serie",
                        fields: {
                            name: { editable: false, nullable: true }
                        }
                    }
                }
            }

    });
}


//Method to include DropDownList for the Locations
function costMethodDropDownEditor(container, options) {
    var gridData = [
        { Name: "AVERAGE", Id: 1 },
        { Name: "FIFO", Id: 2 },
        { Name: "LIFO", Id: 3 }
    ];

    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            dataTextField: "Name",
            dataValueField: "Id",
            valuePrimitive: true,
            dataSource: gridData
            /*dataSource: {
                type: "odata",
                transport: {
                    read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories"
                }
            }*/

    });
}

     
});


$(document).ready(function() {
   
  $("#gridApplications").kendoGrid({
    dataSource: {
        transport: {
            read: {
                url: "?Task=ApplicationsController.getAll",
                type: "POST"
            },
            update: {
                url: "?Task=ApplicationsController.upd",
                type: "POST",
                complete: function(e) {
                    
                    if(e.responseJSON.status == 500){
                        alert("El barcode introducido ya esta siendo utilizado por otro articulo");
                        return false;
                        //$('#ValidationFailModal').modal('Show');
                    } else {
                        $("#gridApplications").data("kendoGrid").dataSource.read(); 
                        var grid = $("#gridApplications").data("kendoGrid");
                        grid.refresh();

                        //window.location ="?Task=ArticuloController";
                    }
                    
                }
            },
            create: {
                url: "?Task=ApplicationsController.add",
                type: "POST",
                complete: function(e) {
                    $("#gridApplications").data("kendoGrid").dataSource.read(); 
                    var grid = $("#gridApplications").data("kendoGrid");
                    grid.refresh();
                 }
            }
        },
        schema: {
            data: "data",
            model: {
                id: "id_app",
                fields: {
                    name: { type:"string", validation: { required: true } },
                    id_company: { type:"string", validation: { required: true } },
                    company_name: { type:"string" },
                    url: { type:"string", validation: { required: true } },
                    id_app_type: { type: "string", validation: { required: true } },
                    id_app_type_name: { type:"string" },
                    prefijo: { type:"string" },
                    active: { type:"string", validation: { required: true } },
                    active_name: { type:"string" }
                }
            }
        },
        pageSize: 100
    },
    height: 550,
    sortable: true,
    noRecords: true,
    filterable: true,
    reordenable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    toolbar: [
        { name:"create", text:"Crear Nuevo" },
        { name:"excel", text:"Exportar a Excel" }
    ],
    excel: {
        fileName: "Applicaciones.xlsx"
    },
    editable: "inline",
    columns: [{
        field: "id_app",
        title: "Código",
        width: 110,
        hidden: true
    },{
        field: "name",
        title: "Nombre",
        width: 200
    },{
        field: "id_company",
        title: "Compañia",
        width: 180,
        editor: companyDropDownEditor,
        template: "#=company_name#"
    },{
        field: "company_name",
        title: "company_name",
        width: 180,
        hidden: true
    },{
        field: "url",
        title: "Ruta",
        width: 250
    },{
        field: "id_app_type",
        title: "Tipo Aplicación",
        width: 180,
        editor: appTypesDropDownEditor,
        template: "#=id_app_type_name#"
    },{
        field: "id_app_type_name",
        title: "id_app_type_name",
        width: 180,
        hidden: true
    },{
        field: "prefijo",
        title: "Prefijo",
        width: 110
    },{
        field: "active",
        title: "Estado",
        width: 180,
        editor: activeDropDownEditor,
        template: "#=active_name#"
    },{
        field: "active_name",
        title: "Estado",
        width: 180,
        hidden: true
    },{ 
        /*command: ["edit", "destroy"], */
        command: [
                    { name:"edit", text: { edit:"Actualizar", update: "Actualizar", cancel: "Cancelar"} }
                ],
        title: "&nbsp;", 
        width: "250px" 
    }]
  });


  //Method to include DropDownList for the Locations
    function companyDropDownEditor(container, options) {
        //alert(options.field);
        $('<input data-text-field="name" data-value-field="id_company" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id_company",
                value: options.model.companyId,
                dataSource: {
                    transport: {
                        read: "?Task=ApplicationsController.getCompanies",
                    },
                    schema: {
                        data: "data",
                        model: {
                            id: "id_company",
                            fields: {
                                name: { editable: false, nullable: true }
                            }
                        }
                    }
                }

        });
    }

    //Method to include DropDownList for the Units
    function appTypesDropDownEditor(container, options) {

        $('<input required name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id_app_type",
                dataSource: {
                    transport: {
                        read: "?Task=ApplicationsController.getApplicationsTypes",
                    },
                    schema: {
                        data: "data",
                        model: {
                            id: "id_app_type",
                            fields: {
                                name: { editable: false, nullable: true }
                            }
                        }
                    }
                }

        });
    }


    //Method to include DropDownList
    function activeDropDownEditor(container, options) {

        var gridData = [
            { Name: "Active", Id: 1 },
            { Name: "Inactive", Id: 0 }
        ];

        $('<input required name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Id",
                dataSource: gridData
                /*dataSource: {
                    type: "odata",
                    transport: {
                        read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories"
                    }
                }*/

        });
    }

   
});


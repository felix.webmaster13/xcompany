<?php

use Libraries\CommonView;
use app\Models\SuplierModel;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class OnhandReportView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'OnhandReport';
    $title = 'Reporte de Existencia Actual';
    //$this -> boddy($title, $html);
    $this -> template($title, $html);
    
    
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('OnhandReport');

    
  }

}


?>
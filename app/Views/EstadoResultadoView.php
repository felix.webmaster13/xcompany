<?php

use Libraries\CommonView;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class EstadoResultadoView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'EstadoResultado';
    $title = 'Estado de Resultado';
    $this -> template($title, $html);
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('EstadoResultado');

    

  }

}


?>
<?php

use Libraries\CommonView;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class CompaniesView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'companies';
    $title = '';
    $this -> header();
    $this -> boddy2($title, $html,1);
    
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('Companies');

  }

}


?>
<?php

use Libraries\CommonView;
use app\Models\SuplierModel;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class DevolucionSalidaView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'DevolucionSalida';
    $title = 'Devolución de Venta';
    //$this -> boddy($title, $html);
    $this -> template($title, $html);
    
    
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('DevolucionSalida');

    
    
  }

}


?>
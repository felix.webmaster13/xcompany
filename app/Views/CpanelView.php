<?php

use Libraries\CommonView;

/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class CpanelView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'cpanel';
    $title = 'Welcome';
    $this -> template($title, $html);
  }

  public function index($viewName){
    //Loading JS
    echo '<script language="javascript" type="text/javascript">';
      require_once '../app/js/cpanel.js';
    echo '</script>';

  }

}


?>
<?php

use Libraries\CommonView;
use app\Models\SuplierModel;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class Report606View extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'Report606';
    $title = 'Reporte 606';
    //$this -> boddy($title, $html);
    $this -> template($title, $html);
    
    
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('Report606');

    
  }

}


?>
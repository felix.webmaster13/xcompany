<?php

/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class loginView{

  public function __construct(){
    //echo "Clase View";
    //Loading
  }

  public function index($viewName){
    //Loading template
      require_once 'app/login.php';
    //Loading JS
    echo '<script language="javascript" type="text/javascript">';
      require_once '../app/js/login.js';
    echo '</script>';

  }

}


?>
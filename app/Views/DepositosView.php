<?php

use Libraries\CommonView;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class DepositosView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'Depositos';
    $title = 'Registro de Depósitos';
    $this -> template($title, $html);
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('Depositos');

    

  }

}


?>
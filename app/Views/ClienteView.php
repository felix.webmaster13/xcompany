<?php

use Libraries\CommonView;


/*
 * Class to display the HTML view to the user
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Abril del 2020 
 * 
 */
Class ClienteView extends CommonView{

  public function __construct(){
    //Loading template
    $html = 'Cliente';
    $title = 'Registro de Clientes';
    $this -> template($title, $html);
  }

  public function index($viewName){
    //Loading JS
    $this -> addJS('Cliente');

    

  }

}


?>
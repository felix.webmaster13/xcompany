<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\UsersPermmisionsModel;
use app\Models\UsersModel;
//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 17 de Junio del 2020 
 * 
 */
Class UsersPermmisionsController extends Controller{

    private $session;

    public function __construct(){
        //echo "Controllador IndexController";
        $this -> model = new UsersPermmisionsModel();

        //Review session status
        $this -> session = new Session();
        $this -> session -> initialize();
        if( $this -> session -> getStatus() === 1 || empty( $this -> session -> get('user_id') ) ){
            //exit('Access Deny');
            $this -> RedirectSessionDeny();
        }
        
    }

    public function index(){
        $this -> View('UsersPermmisionsView');
    }

    //Method to add record into a table
    public function add(){
       
        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        
        $data -> id_user    = $input -> getInteger('id_user');
        
        if(isset($_POST['modules'])){
            $data -> modules    = $_POST['modules'];
            $data -> modulesval = $_POST['modulesval'];
        }

        //print_r($_POST['modules']);
        //print_r($input -> getArray('modules'));
        //exit;
        
        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> save( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }

    }

    //Method to add record into a table
    public function upd(){
       //echo "Actualizar";
        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        $data -> id_role     = $input -> getInteger('id_role');
        $data -> name        = $input -> getString('name');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> update( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }

    }


    //Method to load combobox of all suppliers
    public function getUsers(){

        $response = new JSONResponse();
        $db = new UsersModel();

        try {
            
            $dataModel = $db -> getAllActive();
            //print_r($dataModel);
            if( count($dataModel) >= 1 ){
                if( is_numeric($dataModel[0]['user_id']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                }
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }
    }
 
    //Method to get Applications by Company
    public function getPermisionsByUser(){
        
        $input = new Input();
        $response = new JSONResponse();
        $db = $this -> model;


        $data = new StdClass();
        $data -> user_id     = $input -> getInteger('user_id');

        try {
            
            $dataModel = $db -> getPermisionsByUser($data);
            //print_r($dataModel);
           // exit;
            //echo $dataModel[0]['id_articulo'];
            if(count($dataModel)>=1){
                if( is_numeric($dataModel[0]['id_app']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                } 
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }

       
    }

    

}

?>
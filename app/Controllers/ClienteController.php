<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\ClienteModel;
use app\Models\TipoNCFModel;

//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 09 de Abril del 2020 
 * 
 */
Class ClienteController extends Controller{

    private $session;

    public function __construct(){
        //echo "Controllador IndexController";
        $this -> model = new ClienteModel();

        //Review session status
        $this -> session = new Session();
        $this -> session -> initialize();
        if( $this -> session -> getStatus() === 1 || empty( $this -> session -> get('user_id') ) ){
            //exit('Access Deny');
            $this -> RedirectSessionDeny();
        }
        
    }

    public function index(){
        $this -> View('ClienteView');
    }


    //Method to add record into a table
    public function add(){
       
        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        
        $data -> id_cliente     = $input -> getInteger('id_cliente');
        $data -> name           = $input -> getString('name');
        $data -> rnc            = $input -> getInteger('rnc');
        $data -> phone          = $input -> getString('phone');
        $data -> email          = $input -> getString('email');
        $data -> id_tipo_ncf    = $input -> getInteger('id_tipo_ncf');
        $data -> active         = $input -> getInteger('active');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> save( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }

    }

    //Method to add record into a table
    public function upd(){
       //echo "Actualizar";
       /* $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        $data -> id_cliente  = $input -> getInteger('id_cliente');
        $data -> name        = $input -> getString('name');
        $data -> rnc         = $input -> getString('rnc');
        $data -> phone       = $input -> getString('phone');
        $data -> email       = $input -> getString('email');
        $data -> address     = $input -> getString('address');
        $data -> active      = $input -> getInteger('active');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> update( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }*/

    }

    //Method to get Applications by Company
    public function getAll(){
        $response = new JSONResponse();
        $db = $this -> model;

        try {
            
            $dataModel = $db -> getAll();
            //print_r($dataModel);
            //echo count($dataModel);
            if( count($dataModel) >= 1 ){
                if( is_numeric($dataModel[0]['id_cliente'])){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                 }
            } else {
               echo $response -> json_response(200, null, $dataModel);
            } 

        } catch (Exception $e){
            echo $e->getMessage();
        }
     
    }



     //Method to load combobox of all suppliers
     public function getTipoNCF(){

        $response = new JSONResponse();
        $db = new TipoNCFModel();

        try {
            
            $dataModel = $db -> getAll();
            //print_r($dataModel);
            if( count($dataModel) >= 1 ){
                if( is_numeric($dataModel[0]['id_tipo_ncf']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                }
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }
    }


    //Method to get Applications by Company
    public function getById(){

        $input = new Input();
        $response = new JSONResponse();


        $data = new StdClass();
        $data -> id_articulo   = $input -> getInteger('id_articulo');


        $db = $this -> model;

        try {
            
            $dataModel = $db -> getById($data -> id_articulo);
            //print_r($dataModel);
           // exit;
            //echo $dataModel[0]['id_articulo'];
            if(count($dataModel)>=1){
                if( is_numeric($dataModel[0]['id_cliente']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                } 
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }
  
    }

    

}

?>
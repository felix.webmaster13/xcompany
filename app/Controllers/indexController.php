<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\IndexModel;
//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 09 de Abril del 2020 
 * 
 */
Class indexController extends Controller{

    //protected $model;
    private $session;

    public function __construct(){
        //echo "Controllador IndexController";
        $this -> model = new IndexModel();
        $this -> session = new Session(); 
       //$this -> db = 55;       
        
    }

    public function index(){
        $this -> View('loginView');
    }

    public function save(){

        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        $data -> username = $input -> getString('user');
        $data -> password = $input -> getString('pass');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {

            $dataModel = $db -> save( $data );
            //print_r( $dataModel );
           
            if( isset($dataModel['user_id']) ){
                echo $response -> json_response(200, null, $dataModel);

                 //Inizializing user session
                 $this -> session -> initialize();
                 $this -> session -> addToSession('user_id', $dataModel['user_id']);
                 $this -> session -> addToSession('name', $dataModel['name']);
                 //print_r($this -> session -> getAll());
                 //header("location: ?Task=CpanelController");

            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }
        
    }


    public function logoutSystem(){
        $this -> session -> initialize();
        $this -> session -> close();
        $this -> RedirectSessionDeny();
    }

}

?>
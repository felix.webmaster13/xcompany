<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\RolesModel;
//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 16 de Junio del 2020 
 * 
 */
Class RolesController extends Controller{

    private $session;

    public function __construct(){
        //echo "Controllador IndexController";
        $this -> model = new RolesModel();

        //Review session status
        $this -> session = new Session();
        $this -> session -> initialize();
        if( $this -> session -> getStatus() === 1 || empty( $this -> session -> get('user_id') ) ){
            //exit('Access Deny');
            $this -> RedirectSessionDeny();
        }
        
    }

    public function index(){
        $this -> View('RolesView');
    }

    //Method to add record into a table
    public function add(){
       
        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        
        $data -> name           = $input -> getString('name');
        
        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> save( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }

    }

    //Method to add record into a table
    public function upd(){
       //echo "Actualizar";
        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        $data -> id_role     = $input -> getInteger('id_role');
        $data -> name        = $input -> getString('name');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {
            //print_r($data);
            $dataModel = $db -> update( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, null, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }

    }

 
    //Method to get Applications by Company
    public function getAll(){
        $response = new JSONResponse();
        $db = $this -> model;

        try {
            
            $dataModel = $db -> getAll();
            //print_r($dataModel);
           // exit;
            //echo $dataModel[0]['id_articulo'];
            if(count($dataModel)>=1){
                if( is_numeric($dataModel[0]['id_role']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                } 
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }

       
    }

    

}

?>
<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\IndexModel;
//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 09 de Abril del 2020 
 * 
 */
Class CpanelController extends Controller{

    private $session;

    public function __construct(){
        //echo "Controllador IndexController";
        
        $this -> model = new IndexModel();
        
        //Review session status
        $this -> session = new Session();
        $this -> session -> initialize();
        if( $this -> session -> getStatus() === 1 || empty( $this -> session -> get('user_id') ) ){
            //exit('Access Deny');
            $this -> RedirectSessionDeny();
        }
        
    }

    public function index( $company_id = '' ){
        /*echo $company_id."============Company";
        exit;*/
        if(!empty($company_id)){
            $_SESSION['COMPANY_ID'] =  $company_id;
            $this -> session -> addToSession('company_id', $company_id);
        }
        $this -> View('CpanelView');
    }

    public function save(){

        $input = new Input();
        $response = new JSONResponse();
       
        $data = new StdClass();
        $data -> username = $input -> getString('user');
        $data -> password = $input -> getString('pass');

        //Accesing into a Model 
        $db = $this -> model;
        
        try {

            $dataModel = $db -> save( $data );
            if(!$dataModel){
                 echo $response -> json_response(200, $dataModel);
            } else {
                echo $response -> json_response(500, $dataModel);
            } 
        
        } catch (Exception $e){
            echo $e->getMessage();
        }
        
        
        
        

    }

    

}

?>
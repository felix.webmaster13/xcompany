<?php

use Libraries\Controller;
use Libraries\Input;
use Libraries\JSONResponse;
use Libraries\Session;
use app\Models\CompaniesModel;
//use Exception;
  

/*
 * 
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 09 de Abril del 2020 
 * 
 */
Class CompaniesController extends Controller{

    private $session;
    private $user_id;
    private $company_id;

    public function __construct(){
        //echo "Controllador IndexController";
        $this -> model = new CompaniesModel();

        //Review session status
        $this -> session = new Session();
        $this -> session -> initialize();
        if( $this -> session -> getStatus() === 1 || empty( $this -> session -> get('user_id') ) ){
            //exit('Access Deny');
            $this -> RedirectSessionDeny();
        } else {
            //print_r($this -> session -> getAll());
            $this -> user_id = $this -> session -> get('user_id'); 
            $this -> company_id = $this -> session -> get('company_id');
            //$this -> session ->getAll(); 
        }
        
    }

    public function index(){
        $this -> View('CompaniesView');
    }

    //Method to get Applications by Company
    public function getApplications(){
        $input = new Input();
        $response = new JSONResponse();

        //$company =  1;
        //Setting up object with variables
        $data = new StdClass();
        $data -> id_company = $this -> company_id;

        $db = $this -> model;

        try {
            
            $dataModel = $db -> getApplicationsByCompany( $data );
            //print_r($dataModel);
            if( isset($dataModel[0] -> id_app) ){
                echo $response -> json_response(200, null, $dataModel);
           } else {
               echo $response -> json_response(500, $dataModel);
           } 

        } catch (Exception $e){
            echo $e->getMessage();
        }

       
    }


    //Method to load combobox of all Companies
    public function getCompanies(){

        $response = new JSONResponse();

        //Setting up object with variables
        $data = new StdClass();
        $data -> id_company = $this -> company_id;
        $data -> user_id    = $this -> user_id;

        $db = $this -> model;

        try {
            
            $dataModel = $db -> getAllByAccess($data);
            
            if( count($dataModel) >= 1 ){
                if( is_numeric($dataModel[0]['id_company']) ){
                    $datos2 = [];
                    for($i=0; $i<=count($dataModel)-1;$i++){
                        $datos2[$i] =$dataModel[$i];
                    }
                    echo $response -> json_response(200, null, $datos2);
                } else {
                    echo $response -> json_response(500, $dataModel);
                }
            } else {
                echo $response -> json_response(200, null, $dataModel);
            }

        } catch (Exception $e){
            echo $e->getMessage();
        }
    }


    

}

?>
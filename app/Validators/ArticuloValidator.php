<?php
namespace app\Validators;

use Exception;


/*
 * 
 * Class inchange of validate the data before save into de database model
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 12 de Mayo del 2020 
 * 
 */
Class ArticuloValidator{

    protected $input;
    protected $model;

    public function __construct( $model, $data ){

        $this -> input = $data;
        $this -> model = $model;

    }

    public function valid(){

        //print_r($this -> input);
        
        //call function to verify if the onhand it is enough for the request quantity
        $result = $this -> model -> reviewBarcode( $this -> input -> barcode, $this -> input -> id_articulo);
       
        if($result){
            
            throw new Exception("Validation Fail");
            return false;
        
        } 
            
        //print_r($result);
        return true;

        
    }

}


?>

<?php
namespace app\Validators;

use Exception;


/*
 * 
 * Class inchange of validate the data before save into de database model
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 12 de Mayo del 2020 
 * 
 */
Class SupplierValidator{

    protected $input;
    protected $model;

    public function __construct( $model, $data ){

        $this -> input = $data;
        $this -> model = $model;

    }

    public function valid(){

        //print_r($this -> input);
        
        //call function to verify if the rnc typed was alredy save
        $result = $this -> model -> reviewRNC( $this -> input -> rnc, $this -> input -> id_suplidor);
       //print_r($result);
        if($result){
            
            throw new Exception("Validation Fail");
            return false;
        
        } 
            
        //print_r($result);
        return true;

        
    }


    public function validTMP(){

        //print_r($this -> input);
        
        //call function to verify if the onhand it is enough for the request quantity
        $result = $this -> model -> reviewItemsOnhandWithTMP( $this -> input -> id_salida_detalle, $this -> input -> cantidad);
       
        if($result){
            
            throw new Exception("Validation Fail");
            return false;
        
        } 
            
        //print_r($result);
        return true;

        
    }


}


?>

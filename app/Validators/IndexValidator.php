<?php
namespace app\Validators;

use Exception;


/*
 * 
 * Class inchange of validate the data before save into de database model
 * @params = N/A
 * Autor: FVSoftweare - Felix Valerio
 * Last modified Date: 21 de Abril del 2020 
 * 
 */
Class IndexValidator{

    protected $input;
    protected $model;

    public function __construct( $model, $data ){

        $this -> input = $data;
        $this -> model = $model;

    }

    public function valid(){

        
        //call function to verify data in the database
        $result = $this -> model -> validateUserLogin( $this -> input -> username, $this -> input -> password);
        
        if(!$result){
            
            throw new Exception("Validation Fail");
            return false;
        
        } 
            
        //print_r($result);
        return true;

        
    }


}


?>

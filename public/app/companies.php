 
<!------ Modal displayed if the validation Fail---->
<div class="modal fade" id="programModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-target="#programModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select Company</h5>
        </div>
        <div class="modal-body">Por favor, selecciona la compañia con la que se va a trabajar.</div>
        <div class="modal-body">
          <select name="cmbCompany" id="cmbCompany" required>
          </select>
        </div>
        <div class="modal-footer">
          <button id="btnOK" name="btnOK" class="btn btn-secondary" type="button" data-dismiss="modal">OK</button>
          <!---<a class="btn btn-primary" href="../../index.php">Logout</a>-->
        </div>
      </div>
    </div>
  </div>
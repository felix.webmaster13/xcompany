<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>FVSoftware - Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo PATH_URL; ?>/public/img/logooficialicon.png" rel="icon">
  <link href="<?php echo PATH_URL; ?>/public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo PATH_URL; ?>/public/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-6 col-lg-6 col-md-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="p-5">
                <div class="col-lg-12 text-center" >
                  <img src="public/img/logooficial.png" height="150px" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 d-none"></div>
              <div class="col-lg-12">
                <div class="p-2">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome!</h1>
                  </div>
                  <form class="user" id="frmlogin">
                    <div class="form-group">
                      <input type="text" name="username" id="username" require class="form-control form-control-user" placeholder="Enter User Name..." value="" />
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" id="password" require class="form-control form-control-user" placeholder="Password" value="" />
                    </div>
                    <!--<div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <a href="#" id="login" class="btn btn-primary btn-user btn-block">
                      Login
                    </a>-->
                    <input type="submit" name="Login" class="btn btn-primary btn-user btn-block" value="Login" />
                  </form>
                  <hr>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>


<!------ Modal displayed if the validation Fail---->
  <div class="modal fade" id="ValidFailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-target="#ValidFailModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Validation Fail</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">User name or Password typed it is invalid.<br /> 
        Please hit OK and try again.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">OK</button>
          <!---<a class="btn btn-primary" href="../../index.php">Logout</a>-->
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo PATH_URL; ?>/public/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo PATH_URL; ?>/public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo PATH_URL; ?>/public/vendor/jquery-validator/jquery.validate.min.js"></script>
  


  <!-- Core plugin JavaScript-->
  <script src="<?php echo PATH_URL; ?>/public/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo PATH_URL; ?>/public/js/sb-admin-2.min.js"></script>

</body>

</html>
